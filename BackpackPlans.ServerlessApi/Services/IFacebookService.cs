using System.Threading.Tasks;
using BackpackPlans.Data.Models.Domain;

namespace BackpackPlans.ServerlessApi.Services
{
    public interface IFacebookService
    {
        Task<DynamoFbUser> GetUserFromFacebookAsync(string facebookToken);
    }
}