using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BackpackPlans.ServerlessApi.Helpers;
using BackpackPlans.Data.Models.Domain;
using BackpackPlans.Data.Repositories;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace BackpackPlans.ServerlessApi.Services
{
    public class UserService : IUserService
    {
        private readonly IFacebookService facebookService;
        private readonly AppSettings appSettings;
        private readonly IUserRepository userRepository;
        private readonly IAuthenticationHelper authenticationHelper;

        public UserService(IFacebookService facebookService, IOptions<AppSettings> appSettings, IUserRepository userRepository, IAuthenticationHelper authenticationHelper)
        {
            this.facebookService = facebookService;
            this.appSettings = appSettings.Value;
            this.userRepository = userRepository;
            this.authenticationHelper = authenticationHelper;
        }

        public async Task<DynamoFbUser> Authenticate(string authToken)
        {
            // TODO: handle exceptions from the service in case token is not valid
            var user = await facebookService.GetUserFromFacebookAsync(authToken);

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Email)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            return user;
        }

        public async Task<bool> ChangeBackPacks(int count, int userId)
        {
            return await userRepository.ChangeBackPacks(count, userId);
        }

        public User GetUserDetail(string email)
        {
            // TODO: make async
            return userRepository.GetUserDetail(email);
        }

        public async Task<bool> SaveUser(User user)
        {
            // TODO: make async
            // TODO: update DynamoDB user entry with user ID
            // TODO: user photo get broken because it uses temporary link to FB site. We should download the photo and store it in S3 maybe
            return userRepository.SaveUser(user);
        }
    }
}