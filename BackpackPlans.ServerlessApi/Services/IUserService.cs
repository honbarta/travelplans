using System.Threading.Tasks;
using BackpackPlans.Data.Models.Domain;

namespace BackpackPlans.ServerlessApi.Services
{
  public interface IUserService
    {
        Task<DynamoFbUser> Authenticate(string username);
        Task<bool> ChangeBackPacks(int count, int userId);
        Task<bool> SaveUser(User user);
        User GetUserDetail(string email);
    }
}