using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using BackpackPlans.Data.Models.Domain;
using BackpackPlans.Data.Repositories;
using Newtonsoft.Json;

namespace BackpackPlans.ServerlessApi.Services
{
    public class FbUser
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public FbUserPicture picture { get; set; }

        public class FbUserPicture
        {
            public FbUserPictureData data { get; set; }

            public class FbUserPictureData
            {
                public string url { get; set; }
            }
        }
    }

    public class FacebookService : IFacebookService
    {
        private readonly HttpClient _httpClient;
        private readonly IDynamoDbUserRepository userRepository;

        public FacebookService(IDynamoDbUserRepository userRepository)
        {
            this.userRepository = userRepository;

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://graph.facebook.com/v2.8/")
            };
            _httpClient.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }


        public async Task<DynamoFbUser> GetUserFromFacebookAsync(string facebookToken)
        {
            var result = await GetAsync<FbUser>(facebookToken, "me", "fields=first_name,last_name,email,picture.width(200).height(200)");
            if (result == null)
            {
                throw new Exception("User from this token not exist");
            }

            var user = await userRepository.GetUser(result.email);

            if (user == null)
            {
                user = new DynamoFbUser {
                    FirstName = result.first_name,
                    LastName = result.last_name,
                    Email = result.email,
                    Icon = result.picture.data.url,
                    FirstLogin = true,
                };

                await userRepository.SaveUser(user);
            }

            // FORDEBUG
            // user.FirstLogin = true;

            return user;
        }

        private async Task<T> GetAsync<T>(string accessToken, string endpoint, string args = null)
        {
            var response = await _httpClient.GetAsync($"{endpoint}?access_token={accessToken}&{args}");
            if (!response.IsSuccessStatusCode)
                return default(T);

            var result = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(result);
        }
    }

}