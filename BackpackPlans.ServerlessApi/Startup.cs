﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using BackpackPlans.ServerlessApi.Helpers;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using BackpackPlans.ServerlessApi.Services;
using BackpackPlans.Data.Repositories;
using Microsoft.EntityFrameworkCore;

namespace BackpackPlans.ServerlessApi
{
  public class Startup
    {
        public const string AppS3BucketKey = "AppS3Bucket";
        readonly string MyAllowSpecificOrigins = "CloudFrontCORS";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static IConfiguration Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            var connectionString = Configuration["DbContextSettings:ConnectionString"];

            services.AddEntityFrameworkNpgsql()
                .AddDbContext<BackpackPlansDBContext>(opts => opts.UseNpgsql(connectionString))
                .BuildServiceProvider();

            services.AddCors();

            services.AddHttpClient();

            // TODO: chain
            // TODO: too long, move to an extension method
            services.AddScoped<IBackpackPlansDBContext, BackpackPlansDBContext>();
            services.AddScoped<ITransaction, Transaction>();
            services.AddTransient<ITransactionProvider, TransactionProvider>();
            services.AddTransient<IPlanRepository, PlanRepository>();
            services.AddScoped<IFacebookService, FacebookService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IAuthenticationHelper, AuthenticationHelper>();
            services.AddScoped<ICommentRepository, CommentRepository>();
            services.AddTransient<IPingRepository, PingRepository>();
            services.AddScoped<IDynamoDbUserRepository, DynamoDbUserRepository>();

            services.AddControllers().AddNewtonsoftJson();

            // Add S3 to the ASP.NET Core dependency injection framework.
            services.AddAWSService<Amazon.S3.IAmazonS3>();

            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
                        {
                            builder
                            .AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader();
                        });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
