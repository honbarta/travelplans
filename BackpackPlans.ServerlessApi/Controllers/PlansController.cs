using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BackpackPlans.Data.Models.Domain;
using BackpackPlans.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using BackpackPlans.ServerlessApi.Helpers;
using Amazon.S3;
using System.IO;
using Amazon.S3.Transfer;
using Amazon;
using Microsoft.AspNetCore.Http;
using Amazon.S3.Model;
using System.Net.Mime;
using Microsoft.AspNetCore.StaticFiles;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Options;

namespace BackpackPlans.ServerlessApi.Controllers
{
    public class UnlockPlanModel
    {
        public int PlanId { get; set; }
    }

    public class UnlockPlanReturnModel
    {
        public Plan Plan { get; set; }
        public int BackpackCount { get; set; }
    }

    public class UploadImageReturnModel
    {
        public class UploadImagePathReturnModel
        {
            public string FilePath { get; set; }
        }

        public UploadImagePathReturnModel Data { get; set; }
    }


    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class PlansController : ControllerBase
    {
        private readonly IPlanRepository planRepository;
        private readonly ITransactionProvider transactionProvider;
        private readonly IAuthenticationHelper authenticationHelper;
        private readonly IUserRepository userRepository;
        private readonly ICommentRepository commentRepository;

        public PlansController(IPlanRepository planRepository, ITransactionProvider transactionProvider,
            IAuthenticationHelper authenticationHelper, IUserRepository userRepository,
            ICommentRepository commentRepository) {
            this.planRepository = planRepository;
            this.transactionProvider = transactionProvider;
            this.authenticationHelper = authenticationHelper;
            this.userRepository = userRepository;
            this.commentRepository = commentRepository;
        }

        // TODO: async
        [HttpGet]
        public ICollection<Plan> Get([FromQuery(Name = "userId")] int? userId = null)
        {
            return planRepository.GetPlans(userId);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Plan>> Get(int id)
        {
            // TODO: check id and throw

            string userEmail = await authenticationHelper.GetUserEmail(HttpContext);
            int userId = userRepository.GetUserDetail(userEmail).Id;

            var plan = planRepository.GetPlanDetail(id);

            var paidUser = plan.UnlockedPlans.FirstOrDefault(up => up.PlanId == id && up.UserId == userId);

            if (paidUser == null && plan.User.Id != userId)
            {
                plan.UnlockedPlans = null;
                plan.Items = new [] { // TODO: move somewhere else
                    new PlanItem {
                        Id = 1,
                        Description = "You need to unlock this plan first. If you don't have enough BackPacks either add new plan or buy some",
                        Lat = 47.0547496,
                        Lon = 8.2472857,
                        Day = 1,
                        Type = PlanItemType.Day
                    },
                    new PlanItem {
                        Id = 2,
                        Description = "You need to unlock this plan first. If you don't have enough BackPacks either add new plan or buy some",
                        Lat = 47.0547496,
                        Lon = 8.2472857,
                        Day = 1,
                        Type = PlanItemType.Night
                    },
                    new PlanItem {
                        Id = 3,
                        Description = "You need to unlock this plan first. If you don't have enough BackPacks either add new plan or buy some",
                        Lat = 47.0547496,
                        Lon = 8.2472857,
                        Day = 2,
                        Type = PlanItemType.Day
                    }
                };
            }
            else
            {
                plan.UnlockedPlans = new [] { paidUser };
            }

            return plan;
        }

        private int CountWords(string text)
        {
            char[] delimiters = new char[] {' ', '\r', '\n' };
            return text.Split(delimiters,StringSplitOptions.RemoveEmptyEntries).Length;
        }

        private int CountImages(string text)
        {
            Regex regex = new Regex(@"!\[?(.*)\]\(?(.*)\)");
            return regex.Matches(text).Count;
        }

        private void CountWordsImages(Plan plan) // TODO: move to a service
        {
            plan.WordsCount += CountWords(plan.Description);
            plan.ImagesCount += CountImages(plan.Description);

            foreach (var item in plan.Items)
            {
                plan.WordsCount += CountWords(item.Description);
                plan.ImagesCount += CountImages(item.Description);
            }
        }

        [HttpPost]
        public async Task<ActionResult<Plan>> PostAsync([FromBody]Plan plan)
        {
            string userEmail = await authenticationHelper.GetUserEmail(HttpContext);
            int id = userRepository.GetUserDetail(userEmail).Id;

            plan.User = new User {Id = id};
            if (plan.Items == null) {
                plan.Items = new PlanItem[0];
            }

            plan.TripLength = Convert.ToInt32(Math.Round((double)plan.Items.Count / 2, MidpointRounding.AwayFromZero));
            CountWordsImages(plan);

            using(var transaction = transactionProvider.CreateTransaction())
            {
                planRepository.SavePlan(plan);
                await userRepository.ChangeBackPacks(5, id);

                transaction.Commit();
            }

            return plan;
        }

        // TODO: split in multiple methods
        [HttpPut("{id}")]
        public async Task<ActionResult<Plan>> PutAsync([FromBody]Plan plan, int id)
        {
            string userEmail = await authenticationHelper.GetUserEmail(HttpContext);
            int userId = userRepository.GetUserDetail(userEmail).Id;

            var savedPlan = await planRepository.GetPlan(id);

            if (savedPlan == null)
            {
                return NotFound();
            }

            if (savedPlan.UserId != userId)
            {
                return StatusCode(403);
            }

            plan.UserId = userId;
            if (plan.Items == null)
            {
                plan.Items = new PlanItem[0];
            }

            plan.TripLength = Convert.ToInt32(Math.Round((double)plan.Items.Count / 2, MidpointRounding.AwayFromZero));
            CountWordsImages(plan);

            using(var transaction = transactionProvider.CreateTransaction())
            {
                var updatePlanResult = await planRepository.UpdatePlan(savedPlan, plan);

                if (updatePlanResult == false)
                {
                    return BadRequest("Plan update failed");
                }

                foreach (var savedItem in savedPlan.Items) { // TODO: move to a servicxe
                    var item = plan.Items.SingleOrDefault(i => i.Id == savedItem.Id);
                    bool result;
                    if (item != null)
                    {
                        item.PlanId = plan.Id;
                        result = await planRepository.UpdatePlanItem(savedItem, item);
                    }
                    else
                        result = await planRepository.DeletePlanItem(savedItem);

                    if (result == false)
                    {
                        transaction.Rollback();
                        return BadRequest("Plan update failed");
                    }
                }

                foreach (var item in plan.Items.Where(item => item.Id == 0)) {
                    item.PlanId = plan.Id;
                    var result = await planRepository.SavePlanItem(item);
                    if (result == false)
                    {
                        transaction.Rollback();
                        return BadRequest("Plan update failed");
                    }
                }


                foreach (var savedTag in savedPlan.KeyWords) { // TODO: move to a servicxe
                    var item = plan.KeyWords.SingleOrDefault(i => i.Id == savedTag.Id);
                    bool result;
                    if (item != null)
                    {
                        item.PlanId = plan.Id;
                        result = await planRepository.UpdatePlanTag(savedTag, item);
                    }
                    else
                        result = await planRepository.DeletePlanTag(savedTag);

                    if (result == false)
                    {
                        transaction.Rollback();
                        return BadRequest("Plan update failed");
                    }
                }

                foreach (var item in plan.KeyWords.Where(item => item.Id == 0)) {
                    item.PlanId = plan.Id;
                    var result = await planRepository.SavePlanTag(item);
                    if (result == false)
                    {
                        transaction.Rollback();
                        return BadRequest("Plan update failed");
                    }
                }

                await planRepository.SaveChanges();

                transaction.Commit();
            }

            return plan;
        }

        [HttpPost("Unlock")]
        public async Task<ActionResult<Plan>> UnlockPlan([FromBody]UnlockPlanModel model)
        {
            string userEmail = await authenticationHelper.GetUserEmail(HttpContext);
            int userId = userRepository.GetUserDetail(userEmail).Id;
            var planUser = await planRepository.UnlockPlan(model.PlanId, userId);

            if (planUser.plan == null)
                return BadRequest(new { message = "Plan not unlocked" });

            return Ok((new UnlockPlanReturnModel{ Plan = planUser.plan, BackpackCount = planUser.user.BackpacksCount }));
        }

        [HttpGet("tags")]
        public async Task<ActionResult<PlanTag>> GetTags()
        {
            var tags = await planRepository.GetTags();
            return Ok(tags);
        }

        [HttpPost("{id}/comments")]
        public async Task<ActionResult<Comment>> PostComment([FromBody]Comment comment, int id)
        {
            string userEmail = await authenticationHelper.GetUserEmail(HttpContext);
            int userId = userRepository.GetUserDetail(userEmail).Id;
            comment.User = new User { Id = userId };
            comment.PlanId = id;
            var result = await commentRepository.SaveComment(comment);

            if (result == false)
            {
                return BadRequest(new { message = "Could not create comment" });
            }

            return Ok(comment);
        }

        [HttpPost("{id}/likes")]
        public async Task<ActionResult> PostLike(int id)
        {
            string userEmail = await authenticationHelper.GetUserEmail(HttpContext);
            int userId = userRepository.GetUserDetail(userEmail).Id;

            using(var transaction = transactionProvider.CreateTransaction())
            {
                var likeResult = await planRepository.LikePlan(id, userId);

                if (likeResult.result == false)
                {
                    transaction.Rollback();
                    return BadRequest(new { message = "This user already liked this plan" });
                }

                var increaseBackpacksResult = await userRepository.ChangeBackPacks(1, likeResult.planOwnerId);

                if (increaseBackpacksResult == false)
                {
                    transaction.Rollback();
                    return BadRequest(new { message = "Could not like this plan" });
                }

                transaction.Commit();
            }

            return Ok();
        }
    }
}
