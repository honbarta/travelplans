using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using BackpackPlans.Data.Models.Domain;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Options;
using BackpackPlans.ServerlessApi.Helpers;

namespace BackpackPlans.ServerlessApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PlacesController : ControllerBase
    {
        private readonly HttpClient httpClient;
        private readonly AppSettings appSettings;

        public PlacesController(IOptions<AppSettings> appSettings)
        {
            this.appSettings = appSettings.Value;

            httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        // TODO: async
        [HttpGet("search/{place}")]
        public async Task<JObject> Get(string place)
        {
            var response = await httpClient.GetAsync($"https://api.tomtom.com/search/2/search/{place}.json?idxSet=Geo&key={appSettings.TomTomKey}&limit=40");
            Console.WriteLine(response);
            if (!response.IsSuccessStatusCode)
                return new JObject();

            var result = await response.Content.ReadAsStringAsync();

            return JObject.Parse(result); ;
        }

        [HttpGet("foursquare/{lon}/{lat}")]
        public async Task<JObject> GetFoursquare(string lon, string lat)
        {
            var url = $"https://api.foursquare.com/v2/venues/explore?ll={lon},{lat}&categoryId=4d4b7104d754a06370d81259&sortByPopularity=1&client_id={appSettings.FoursquareClientID}&client_secret={appSettings.FoursquareClientSecret}&v=20200917";
            return await fetchFoursquareApi(url);
        }

        [HttpGet("foursquare/{place}")]
        public async Task<JObject> GetFoursquare(string place)
        {
            var url = $"https://api.foursquare.com/v2/venues/explore?near={place}&categoryId=4d4b7104d754a06370d81259&sortByPopularity=1&client_id={appSettings.FoursquareClientID}&client_secret={appSettings.FoursquareClientSecret}&v=20200917";
            return await fetchFoursquareApi(url);
        }

        private async Task<JObject> fetchFoursquareApi(string url)
        {
            var response = await httpClient.GetAsync(url);
            Console.WriteLine(response);
            Console.WriteLine(url);
            if (!response.IsSuccessStatusCode)
                return new JObject();

            var result = await response.Content.ReadAsStringAsync();

            return JObject.Parse(result);
        }

    }
}
