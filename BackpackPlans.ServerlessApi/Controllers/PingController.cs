﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackpackPlans.Data.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace BackpackPlans.ServerlessApi.Controllers
{
    [Route("api/[controller]")]
    public class PingController : ControllerBase
    {
        private readonly IPingRepository pingRepository;
        public PingController(IPingRepository pingRepository)
        {
            this.pingRepository = pingRepository;
        }

        // GET api/values
        [HttpGet("server")]
        public string GetServer()
        {
            return "OK";
        }

        // GET api/values
        [HttpGet("db")]
        public string GetDb()
        {
            pingRepository.Ping();
            return "OK";
        }
    }
}
