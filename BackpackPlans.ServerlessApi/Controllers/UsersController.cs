using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BackpackPlans.ServerlessApi.Services;
using BackpackPlans.Data.Models.Domain;
using System.Threading.Tasks;
using BackpackPlans.ServerlessApi.Helpers;

namespace BackpackPlans.ServerlessApi.Controllers
{
    public class FbTokenModel
    {
        public string Token { get; set; }
    }

    public class BuyBackpacksModel
    {
        public int Count { get; set; }
    }


    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IAuthenticationHelper authenticationHelper;

        public UsersController(IUserService userService, IAuthenticationHelper authenticationHelper)
        {
            this.userService = userService;
            this.authenticationHelper = authenticationHelper;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody]FbTokenModel fbToken)
        {
            var user = await userService.Authenticate(fbToken.Token);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(user);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> BuyBackpacks([FromBody]BuyBackpacksModel model)
        {
            string userEmail = await authenticationHelper.GetUserEmail(HttpContext);
            int userId = userService.GetUserDetail(userEmail).Id;
            var result = await userService.ChangeBackPacks(model.Count, userId);

            if (result == false) {
                return BadRequest(new { message = "Transaction failed" });
            }

            return Ok(model);
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]User user)
        {
            user.BackpacksCount = 5; // start with 5 backpacks for the registration

            await userService.SaveUser(user);

            return Ok(user);
        }
    }
}