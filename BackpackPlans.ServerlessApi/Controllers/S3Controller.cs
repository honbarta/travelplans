﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using Amazon.S3.Model;
using Microsoft.AspNetCore.Http;
using BackpackPlans.ServerlessApi.Helpers;
using Microsoft.Extensions.Options;
using System.IO;
using System.Net.Http;
using Microsoft.AspNetCore.Authorization;

namespace BackpackPlans.ServerlessApi.Controllers
{
    [Route("api/[controller]")]
    public class S3Controller : ControllerBase
    {
        private readonly IAuthenticationHelper authenticationHelper;
        private readonly AppSettings appSettings;
        private readonly IHttpClientFactory clientFactory;

        public S3Controller(IAuthenticationHelper authenticationHelper, IOptions<AppSettings> appSettings, IHttpClientFactory clientFactory)
        {
            this.authenticationHelper = authenticationHelper;
            this.appSettings = appSettings.Value;
            this.clientFactory = clientFactory;
        }

        // GET api/values
        [AllowAnonymous] // TODO: add cookie authentication
        [HttpGet("{path}")]
        public async Task<Stream> Get(string path)
        {
            var request = new HttpRequestMessage(HttpMethod.Get,
                $"https://{appSettings.ImagesBucket}-optimized.s3.amazonaws.com/{path}");

            var client = clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStreamAsync();
            }
            else
            {
                return null;
            }
        }

        [HttpPost("upload-image")]
        public async Task<ActionResult<bool>> UploadImage(IFormFile image)
        {
            // TODO: introduce interface and add tests
            using (var client = new AmazonS3Client(appSettings.AwsAccessKeyId, appSettings.AwsAccessKeySecret, RegionEndpoint.USEast1))
            {
                using (var newMemoryStream = new MemoryStream())
                {
                    image.CopyTo(newMemoryStream);

                    var extension = Path.GetExtension(image.FileName);
                    var guid = Guid.NewGuid();
                    var filePath = $"{guid.ToString()}{extension}";

                    var uploadRequest = new TransferUtilityUploadRequest
                    {
                        InputStream = newMemoryStream,
                        Key = filePath,
                        BucketName = appSettings.ImagesBucket,
                        CannedACL = S3CannedACL.PublicRead
                    };

                    var fileTransferUtility = new TransferUtility(client);
                    await fileTransferUtility.UploadAsync(uploadRequest);

                    return Ok(new UploadImageReturnModel { Data = new UploadImageReturnModel.UploadImagePathReturnModel { FilePath = $"api/s3/{filePath}" } });
                };
            }
        }
    }
}
