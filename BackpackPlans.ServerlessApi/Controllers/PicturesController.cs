using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using BackpackPlans.Data.Models.Domain;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Options;
using BackpackPlans.ServerlessApi.Helpers;

namespace BackpackPlans.ServerlessApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PicturesController : ControllerBase
    {
        private readonly HttpClient httpClient;
        private readonly AppSettings appSettings;

        public PicturesController(IOptions<AppSettings> appSettings)
        {
            this.appSettings = appSettings.Value;

            httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        [HttpGet("search/{query}")]
        public async Task<JObject> Get(string query)
        {
            var response = await httpClient.GetAsync($"https://api.unsplash.com/search/photos?query={query}&orientation=landscape&client_id={appSettings.UnsplashKey}");
            Console.WriteLine(response);
            if (!response.IsSuccessStatusCode)
                return new JObject();

            var result = await response.Content.ReadAsStringAsync();

            return JObject.Parse(result);;
        }
    }
}
