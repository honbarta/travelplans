namespace BackpackPlans.ServerlessApi.Helpers
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string ImagesBucket { get; set; }
        public string AwsAccessKeyId { get; set; }
        public string AwsAccessKeySecret { get; set; }
        public string TomTomKey { get; set; }
        public string UnsplashKey { get; set; }
        public string FoursquareClientID { get; set; }
        public string FoursquareClientSecret { get; set; }
    }
}