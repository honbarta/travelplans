using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace BackpackPlans.ServerlessApi.Helpers
{
    public interface IAuthenticationHelper
    {
        Task<string> GetUserEmail(HttpContext httpContext);
    }
}