using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;

namespace BackpackPlans.ServerlessApi.Helpers
{
    public class AuthenticationHelper : IAuthenticationHelper
    {
        public async Task<string> GetUserEmail(HttpContext httpContext)
        {
            var authenticateInfo = await httpContext.AuthenticateAsync("Bearer");
            var email = authenticateInfo.Principal.Identity.Name;
            return email;
        }

    }
}