
const AWS = require('aws-sdk');
const Jimp = require('jimp');

const s3 = new AWS.S3();

exports.handler = async (event) => {

    const srcBucket = event.Records[0].s3.bucket.name
    // Object key may have spaces or unicode non-ASCII characters.
    const srcKey    = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, " "))
    const dstBucket = srcBucket
    const dstKey    = 'api/s3/' + srcKey

    const params = {
        Bucket: srcBucket,
        Key: srcKey
    };
    var origImage = await s3.getObject(params).promise();

    const jimpImage = await Jimp.read(origImage.Body)
    await jimpImage.resize(Number(process.env.IMAGE_WIDTH), Jimp.AUTO) // resize
    await jimpImage.quality(Number(process.env.IMAGE_QUALITY)) // set JPEG quality
    const outputBuffer = await jimpImage.getBufferAsync(Jimp.AUTO); // save

    const destParams = {
        Bucket: dstBucket + '-optimized',
        Key: dstKey,
        Body: outputBuffer,
        ContentType: "image"
    };

    const putResult = await s3.putObject(destParams).promise();

    console.log('Successfully resized ' + srcBucket + '/' + srcKey +
        ' and uploaded to ' + dstBucket + '/' + dstKey);
};
