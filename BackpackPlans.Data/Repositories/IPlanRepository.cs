using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BackpackPlans.Data.Models.Domain;

namespace BackpackPlans.Data.Repositories
{
    public interface IPlanRepository
    {
        ICollection<Plan> GetPlans(int? userId);
        Plan GetPlanDetail(int id);
        Task<Plan> GetPlan(int id);
        Boolean SavePlan(Plan plan);
        Task<bool> SavePlanItem(PlanItem planItem);
        Task<bool> UpdatePlanItem(PlanItem oldPlanItem, PlanItem newPlanItem);
        Task<bool> DeletePlanItem(PlanItem item);
        Task<bool> SavePlanTag(PlanTag planTag);
        Task<bool> UpdatePlanTag(PlanTag oldPlanTag, PlanTag newPlanTag);
        Task<bool> DeletePlanTag(PlanTag tag);
        Task<(Plan plan, User user)> UnlockPlan(int planId, int userId);
        Task<bool> UpdatePlan(Plan oldPlan, Plan newPlan);
        Task<ICollection<PlanTag>> GetTags();
        Task<bool> SaveChanges();
        Task<(bool result, int planOwnerId)> LikePlan(int planId, int userId);
    }
}