using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackpackPlans.Data.Models.Domain;
using Microsoft.EntityFrameworkCore;

namespace BackpackPlans.Data.Repositories
{
  public class PingRepository : IPingRepository
  {
    private IBackpackPlansDBContext dbContext;

    public PingRepository(IBackpackPlansDBContext dbContext) {
        this.dbContext = dbContext;
    }

    public void Ping()
    {
      try
      {
          dbContext.ExecuteSqlCommand("SELECT 'Hello'");
      }
      catch (System.Exception)
      {
          // this is a ping for cold start so we expect connection to take long
          // but we do not have to wait for that
          // so when we try to finish the request and close the connection postgres throw an error
          // because we haven't actually finished the request
          // but it's fine, we don't need the response from DB anyway
      }

    }
  }
}