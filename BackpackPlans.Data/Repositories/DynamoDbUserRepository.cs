using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackpackPlans.Data.Models.Domain;
using Microsoft.EntityFrameworkCore;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Amazon;

namespace BackpackPlans.Data.Repositories
{
    public class DynamoDbUserRepository : IDynamoDbUserRepository
    {
        private IBackpackPlansDBContext dbContext;
        private DynamoFbUser DeserializeUser(Dictionary<string, AttributeValue> values)
        {
            var user = new DynamoFbUser
            {
                Email = values["email"].S,
                FirstName = values["firstName"].S,
                LastName = values["lastName"].S,
                Id = Convert.ToInt32(values["id"].N)
            };

            return user;
        }

        private Dictionary<string, AttributeValue> SerializeUser(DynamoFbUser user)
        {
            return new Dictionary<string, AttributeValue>
            {
                { "email", new AttributeValue { S = user.Email } },
                { "firstName", new AttributeValue { S = user.FirstName } },
                { "lastName", new AttributeValue { S = user.LastName } }
            };
        }

        public async Task<DynamoFbUser> GetUser(string email)
        {
            var client = new AmazonDynamoDBClient(RegionEndpoint.USEast1);
            var key = new Dictionary<string, AttributeValue>
            {
                { "email", new AttributeValue { S = email } }
            };
            var response = await client.GetItemAsync("backpackplans_users", key);

            if (response.Item.Count <= 0)
            {
                return null;
            }

            var user = DeserializeUser(response.Item);

            return user;
        }

        public async Task<bool> SaveUser(DynamoFbUser user)
        {
            var client = new AmazonDynamoDBClient(RegionEndpoint.USEast1);
            var userData = SerializeUser(user);
            var response = await client.PutItemAsync("backpackplans_users", userData);

            return response.HttpStatusCode == HttpStatusCode.Created;
        }
    }
}
