using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using BackpackPlans.Data.Models.Domain;
using System.Threading.Tasks;

namespace BackpackPlans.Data.Repositories
{
    public class BackpackPlansDBContext : DbContext, IBackpackPlansDBContext
    {
        public BackpackPlansDBContext(DbContextOptions<BackpackPlansDBContext> options)
            : base(options)
        {
        }

        public BackpackPlansDBContext() : base()
        {
        }

        // prod
        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //     => optionsBuilder.UseNpgsql("Host=raja.db.elephantsql.com;Database=dziobxji;Username=dziobxji;Password=bxaT-n1yh1TjNEcqc0rvcuuhG-aMNIRk");

        // dev
        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //     => optionsBuilder.UseNpgsql("Host=raja.db.elephantsql.com;Database=loxfmcde;Username=loxfmcde;Password=XPHjnB6aAwCZTKKSAHDKCTVicSPiSuyU");

        public DbSet<Plan> Plans { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<PlanItem> PlanItems { get; set; }
        public DbSet<UnlockedPlans> UnlockedPlans { get; set; }
        public DbSet<PlanTag> PlanTags { get; set; }

        public IDbContextTransaction CreateTransaction()
        {
            return this.Database.BeginTransaction();
        }

        public Task<int> ExecuteSqlCommand(string sql)
        {
            return this.Database.ExecuteSqlCommandAsync(sql);
        }
    }
}


