using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackpackPlans.Data.Models.Domain;
using Microsoft.EntityFrameworkCore;

namespace BackpackPlans.Data.Repositories
{
    public class CommentRepository : ICommentRepository
    {
        private IBackpackPlansDBContext dbContext;

        public CommentRepository(IBackpackPlansDBContext dbContext) {
            this.dbContext = dbContext;
        }

        public async Task<bool> SaveComment(Comment comment)
        {
            dbContext.Comments.Add(comment);
            dbContext.Entry(comment.User).State = EntityState.Unchanged;

            return await dbContext.SaveChangesAsync() == 1;
        }
    }
}