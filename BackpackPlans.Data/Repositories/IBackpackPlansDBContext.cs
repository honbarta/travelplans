
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using BackpackPlans.Data.Models.Domain;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Threading.Tasks;
using System.Threading;

namespace BackpackPlans.Data.Repositories {

    public interface IBackpackPlansDBContext
    {
        DbSet<Plan> Plans { get; set; }
        DbSet<PlanItem> PlanItems { get; set; }
        DbSet<User> Users { get; set; }
        DbSet<UnlockedPlans> UnlockedPlans { get; set; }
        DbSet<PlanTag> PlanTags { get; set; }
        DbSet<Comment> Comments { get; set; }

        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));

        IDbContextTransaction CreateTransaction();
        EntityEntry Entry(object entity);
        EntityEntry Remove(object entity);
        Task<int> ExecuteSqlCommand(string sql);
    }
}