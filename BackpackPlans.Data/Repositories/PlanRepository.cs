using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackpackPlans.Data.Models.Domain;
using Microsoft.EntityFrameworkCore;

namespace BackpackPlans.Data.Repositories
{
    public class PlanRepository : IPlanRepository
    {
        private IBackpackPlansDBContext dbContext;

        public PlanRepository(IBackpackPlansDBContext dbContext) {
            this.dbContext = dbContext;
        }

        public async Task<Plan> GetPlan(int id)
        {
            return await dbContext.Plans
                .Include(plan => plan.KeyWords)
                .Include(plan => plan.Items)
                .FirstOrDefaultAsync(p => p.Id == id);
        }

        public Plan GetPlanDetail(int id)
        {
            var dbPlan = dbContext.Plans
                .Include(plan => plan.KeyWords)
                .Include(plan => plan.User)
                .Include(plan => plan.Comments).ThenInclude(comment => comment.User)
                .Include(plan => plan.Items)
                .Include(plan => plan.UnlockedPlans)
                .Include(plan => plan.Likes)
                .First(p => p.Id == id);

            dbPlan.Items = dbPlan.Items.OrderBy(i => i.Day).ThenBy(i => i.Type).ToList();
            dbPlan.Comments = dbPlan.Comments.OrderByDescending(c => c.Id).ToList();

            return dbPlan;
        }

        public ICollection<Plan> GetPlans(int? userId)
        {
            var plans = dbContext.Plans
                .Include(plan => plan.KeyWords)
                .Include(plan => plan.User)
                .Include(plan => plan.Likes);

            if (userId != null) {
                return plans.Where(plan => plan.User.Id == userId).ToList();
            }

            return plans.ToList();
        }

        public bool SavePlan(Plan plan)
        {
            dbContext.Plans.Add(plan);
            dbContext.Entry(plan.User).State = EntityState.Unchanged;

            return dbContext.SaveChanges() == 1;
        }

        public async Task<bool> UpdatePlan(Plan oldPlan, Plan newPlan)
        {
            dbContext.Entry(oldPlan).CurrentValues.SetValues(newPlan);
            return await dbContext.SaveChangesAsync() <= 1;
        }

        public async Task<bool> SavePlanItem(PlanItem planItem)
        {
            dbContext.PlanItems.Add(planItem);
            return await dbContext.SaveChangesAsync() == 1;
        }

        public async Task<bool> UpdatePlanItem(PlanItem oldPlanItem, PlanItem newPlanItem)
        {
            dbContext.Entry(oldPlanItem).CurrentValues.SetValues(newPlanItem);
            return await dbContext.SaveChangesAsync() <= 1;
        }

        public async Task<bool> DeletePlanItem(PlanItem item)
        {
            dbContext.Remove(item);
            return await dbContext.SaveChangesAsync() == 1;
        }

        public async Task<bool> SavePlanTag(PlanTag planTag) // TODO: change the pattern in the whole repo to not to save changes everytime but only once
        {
            dbContext.PlanTags.Add(planTag);
            return true;
        }

        public async Task<bool> UpdatePlanTag(PlanTag oldPlanTag, PlanTag newPlanTag)
        {
            dbContext.Entry(oldPlanTag).CurrentValues.SetValues(newPlanTag);
            return true;
        }

        public async Task<bool> DeletePlanTag(PlanTag tag)
        {
            dbContext.Remove(tag);
            return true;
        }

        public async Task<(Plan plan, User user)> UnlockPlan(int planId, int userId)
        {
            // TODO: move to the service
            var user = await dbContext.Users.FirstAsync(u => u.Id == userId);
            if (user.BackpacksCount <= 0)
            {
                return (null, user);
            }

            var unlockedPlan = new UnlockedPlans { PlanId = planId, UserId = userId };
            await dbContext.UnlockedPlans.AddAsync(unlockedPlan);
            var unlocked = await dbContext.SaveChangesAsync() == 1;

            if (unlocked == false)
            {
                return (null, user);
            }

            // TODO: move to the service
            user.BackpacksCount = user.BackpacksCount - 1;
            await dbContext.SaveChangesAsync(); // TODO: use transaction

            return (plan: GetPlanDetail(planId), user: user);
        }

        public async Task<ICollection<PlanTag>> GetTags()
        {
            return await dbContext.PlanTags
                .GroupBy(t => t.Name)
                .Select(t => new PlanTag
                {
                    Name = t.Key,
                    Count = t.Count()
                })
                .OrderByDescending(t => t.Count)
                .ToListAsync();
        }

        public async Task<bool> SaveChanges() 
        {
            return await dbContext.SaveChangesAsync() > 0;
        }

        public async Task<(bool result, int planOwnerId)> LikePlan(int planId, int userId)
        {
            var plan = await dbContext.Plans.Include(p => p.Likes).FirstAsync(p => p.Id == planId);

            if (plan.Likes.FirstOrDefault(like => like.UserId == userId) != null)
            {
                return (false, -1);
            }

            plan.Likes.Add(new PlanLikes {PlanId = planId, UserId = userId});

            return (await dbContext.SaveChangesAsync() == 1, plan.UserId);
        }
    }
}