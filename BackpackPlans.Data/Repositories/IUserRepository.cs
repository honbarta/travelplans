using System.Threading.Tasks;
using BackpackPlans.Data.Models.Domain;

namespace BackpackPlans.Data.Repositories
{
    public interface IUserRepository
    {
        User GetUserDetail(int id);        
        User GetUserDetail(string email);
        bool SaveUser(User user);
        Task<bool> ChangeBackPacks(int count, int userId);
    }
}