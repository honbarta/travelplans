namespace BackpackPlans.Data.Repositories {
    public class TransactionProvider : ITransactionProvider
    {
        private IBackpackPlansDBContext dbContext;

        public TransactionProvider(IBackpackPlansDBContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public ITransaction CreateTransaction()
        {
            var transaction = this.dbContext.CreateTransaction();
            return new Transaction(transaction);
        }
    }
}