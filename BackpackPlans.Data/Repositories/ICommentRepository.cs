using System.Collections.Generic;
using System.Threading.Tasks;
using BackpackPlans.Data.Models.Domain;

namespace BackpackPlans.Data.Repositories
{
    public interface ICommentRepository
    {
        Task<bool> SaveComment(Comment comment);
    }
}