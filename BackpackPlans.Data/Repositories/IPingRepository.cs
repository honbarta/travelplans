using System.Threading.Tasks;
using BackpackPlans.Data.Models.Domain;

namespace BackpackPlans.Data.Repositories
{
    public interface IPingRepository
    {
        void Ping();
    }
}