using Microsoft.EntityFrameworkCore.Storage;

namespace BackpackPlans.Data.Repositories {

    public class Transaction : ITransaction
    {
        private IDbContextTransaction transaction;

        public Transaction(IDbContextTransaction transaction)
        {
            this.transaction = transaction;
        }

        public void Commit()
        {
            this.transaction.Commit();
        }

        public void Dispose()
        {
            this.transaction.Dispose();
            this.transaction = null;
        }

        public void Rollback()
        {
            this.transaction.Rollback();
        }
    }
}