using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackpackPlans.Data.Models.Domain;
using Microsoft.EntityFrameworkCore;

namespace BackpackPlans.Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        private IBackpackPlansDBContext dbContext;

        public UserRepository(IBackpackPlansDBContext dbContext) {
            this.dbContext = dbContext;
        }

        public User GetUserDetail(int id)
        {
            return dbContext.Users
                .First(p => p.Id == id);
        }

        public User GetUserDetail(string email)
        {
            return dbContext.Users
                .FirstOrDefault(p => p.Email == email);
        }

        public ICollection<User> ListUsers()
        {
            return dbContext.Users.ToList();
        }

        public bool SaveUser(User user)
        {
            dbContext.Users.Add(user);

            return dbContext.SaveChanges() == 1;
        }

        public async Task<bool> ChangeBackPacks(int count, int userId)
        {
            var user = dbContext.Users.First(u => u.Id == userId);
            dbContext.Entry(user).Reload();
            user.BackpacksCount += count;
            return await dbContext.SaveChangesAsync() == 1;
        }
    }
}