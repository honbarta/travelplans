namespace BackpackPlans.Data.Repositories {

    public interface ITransactionProvider
    {
        ITransaction CreateTransaction();
    }
}