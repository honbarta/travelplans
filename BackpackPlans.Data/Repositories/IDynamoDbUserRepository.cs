using System.Threading.Tasks;
using BackpackPlans.Data.Models.Domain;

namespace BackpackPlans.Data.Repositories
{
  public interface IDynamoDbUserRepository
  {
      Task<DynamoFbUser> GetUser(string email);
      Task<bool> SaveUser(DynamoFbUser user);
  }
}