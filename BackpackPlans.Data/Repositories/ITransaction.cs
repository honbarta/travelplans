using System;

namespace BackpackPlans.Data.Repositories {

    public interface ITransaction : IDisposable
    {
        void Commit();
        void Rollback();
    }
}