using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackpackPlans.Data.Models.Domain
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Icon { get; set; }
        public string Email { get; set; }
        public int BackpacksCount { get; set; }
        public ICollection<UnlockedPlans> UnlockedPlans { get; set; }
    }
}