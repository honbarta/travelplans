using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackpackPlans.Data.Models.Domain
{
    public class DynamoFbUser
    {
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Icon { get; set; }
        public string Email { get; set; }
        public int Id { get; set; }

        [NotMapped]
        public string Token { get; set; }
        [NotMapped]
        public bool FirstLogin { get; set; }
    }
}