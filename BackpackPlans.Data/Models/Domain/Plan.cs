using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BackpackPlans.Data.Models.Domain
{
    public class Plan
    {
        [Key]
        [Editable(false)]
        public int Id { get; set; }
        public String Name { get; set; }
        public ICollection<PlanTag> KeyWords { get; set; }
        public int TripLength { get; set; }
        public String Description { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<UnlockedPlans> UnlockedPlans { get; set; }
        public ICollection<PlanItem> Items { get; set; }
        public ICollection<PlanLikes> Likes { get; set; }
        public int WordsCount { get; set; }
        public int ImagesCount { get; set; }
    }
}