using System;
using System.ComponentModel.DataAnnotations;

namespace BackpackPlans.Data.Models.Domain
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        public int PlanId { get; set; }
        public User User { get; set; }
        public string Text { get; set; }
        
    }
}