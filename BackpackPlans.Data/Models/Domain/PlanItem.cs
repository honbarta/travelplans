using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BackpackPlans.Data.Models.Domain
{
    public enum PlanItemType
    {
        Day = 1,
        Night = 2
    }

    public class PlanItem
    {
        [Key]
        public int Id { get; set; }
        public int PlanId { get; set; }
        public int Day { get; set; }
        public PlanItemType Type { get; set; }
        public String Description { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }

    }
}