using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackpackPlans.Data.Models.Domain
{
    public class PlanTag
    {
        [Key]
        public int Id { get; set; }
        public int PlanId { get; set; }
        public string Name { get; set; }

        [NotMapped]
        public int Count { get; set; }
    }
}