using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackpackPlans.Data.Models.Domain
{
    public class PlanLikes
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Plan")]
        public int PlanId { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
    }
}