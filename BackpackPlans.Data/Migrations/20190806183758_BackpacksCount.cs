﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BackpackPlans.Data.Migrations
{
    public partial class BackpacksCount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BackpacksCount",
                table: "Users",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BackpacksCount",
                table: "Users");
        }
    }
}
