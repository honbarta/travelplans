﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BackpackPlans.Data.Migrations
{
    public partial class PlanCounts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ImagesCount",
                table: "Plans",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "WordsCount",
                table: "Plans",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImagesCount",
                table: "Plans");

            migrationBuilder.DropColumn(
                name: "WordsCount",
                table: "Plans");
        }
    }
}
