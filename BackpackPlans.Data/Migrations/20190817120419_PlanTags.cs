﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BackpackPlans.Data.Migrations
{
    public partial class PlanTags : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlanItems_Plans_PlanId",
                table: "PlanItems");

            migrationBuilder.DropForeignKey(
                name: "FK_Plans_Users_UserId",
                table: "Plans");

            migrationBuilder.DropForeignKey(
                name: "FK_UnlockedPlans_Plans_PlanId",
                table: "UnlockedPlans");

            migrationBuilder.DropForeignKey(
                name: "FK_UnlockedPlans_Users_UserId",
                table: "UnlockedPlans");

            migrationBuilder.DropColumn(
                name: "KeyWords",
                table: "Plans");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "UnlockedPlans",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PlanId",
                table: "UnlockedPlans",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Plans",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PlanId",
                table: "PlanItems",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "PlanTags",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    PlanId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanTags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PlanTags_Plans_PlanId",
                        column: x => x.PlanId,
                        principalTable: "Plans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlanTags_PlanId",
                table: "PlanTags",
                column: "PlanId");

            migrationBuilder.AddForeignKey(
                name: "FK_PlanItems_Plans_PlanId",
                table: "PlanItems",
                column: "PlanId",
                principalTable: "Plans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Plans_Users_UserId",
                table: "Plans",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UnlockedPlans_Plans_PlanId",
                table: "UnlockedPlans",
                column: "PlanId",
                principalTable: "Plans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UnlockedPlans_Users_UserId",
                table: "UnlockedPlans",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlanItems_Plans_PlanId",
                table: "PlanItems");

            migrationBuilder.DropForeignKey(
                name: "FK_Plans_Users_UserId",
                table: "Plans");

            migrationBuilder.DropForeignKey(
                name: "FK_UnlockedPlans_Plans_PlanId",
                table: "UnlockedPlans");

            migrationBuilder.DropForeignKey(
                name: "FK_UnlockedPlans_Users_UserId",
                table: "UnlockedPlans");

            migrationBuilder.DropTable(
                name: "PlanTags");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "UnlockedPlans",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PlanId",
                table: "UnlockedPlans",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Plans",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "KeyWords",
                table: "Plans",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PlanId",
                table: "PlanItems",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_PlanItems_Plans_PlanId",
                table: "PlanItems",
                column: "PlanId",
                principalTable: "Plans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Plans_Users_UserId",
                table: "Plans",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UnlockedPlans_Plans_PlanId",
                table: "UnlockedPlans",
                column: "PlanId",
                principalTable: "Plans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UnlockedPlans_Users_UserId",
                table: "UnlockedPlans",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
