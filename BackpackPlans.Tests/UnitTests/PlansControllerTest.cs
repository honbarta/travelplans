using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using BackpackPlans.Controllers;
using BackpackPlans.Data.Models.Domain;
using BackpackPlans.Repositories;
using BackpackPlans.Data.Repositories;
using System.Threading.Tasks;
using BackpackPlans.Api.Helpers;
using BackpackPlans.ServerlessApi.Controllers;
using BackpackPlans.ServerlessApi.Helpers;

namespace BackpackPlans.Tests
{
    [TestClass]
    public class PlansControllerTest
    {
        [TestMethod]
        public void TestShouldReturnPlans()
        {
            var repositoryMock = new PlanRepositoryMock();
            var transactionProviderMock = new Mock<ITransactionProvider>();
            var transactionMock = new Mock<ITransaction>();
            var authenticationHelperMock = new Mock<IAuthenticationHelper>();
            var controller = new PlansController(repositoryMock, transactionProviderMock.Object, authenticationHelperMock.Object);
            
            var plans = controller.Get();

            Assert.IsTrue(plans.Count == 2, "Should return 2 plans");
    
        }

        
        [TestMethod]
        public async Task TestShouldSavePlan()
        {
            // TODO: Mock bearer token

            var repositoryMock = new PlanRepositoryMock();
            var transactionProviderMock = new Mock<ITransactionProvider>();
            var transactionMock = new Mock<ITransaction>();
            var authenticationHelperMock = new Mock<IAuthenticationHelper>();

            transactionProviderMock.Setup<ITransaction>(mock => mock.CreateTransaction()).Returns(transactionMock.Object);

            var controller = new PlansController(repositoryMock, transactionProviderMock.Object, authenticationHelperMock.Object);
            
            var plan = new Plan{Id = 3, Name = "Test 3"};
            await controller.PostAsync(plan);

            transactionProviderMock.Verify(mock => mock.CreateTransaction(), Times.Once());
            transactionMock.Verify(mock => mock.Commit(), Times.Once());
            var plans = controller.Get();
            Assert.IsTrue(plans.Count == 3, "Should create new plan");
        }
    }
}
