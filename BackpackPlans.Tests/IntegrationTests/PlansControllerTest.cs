using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace BackpackPlans.Tests
{
    [TestClass]
    public class PlansControllerIntegrationTest
    {
        private readonly TestServer server;
        private readonly HttpClient client;
        public PlansControllerIntegrationTest()
        {
            this.server = new TestServer(new WebHostBuilder()
                .UseStartup<InMemoryStartup>());
            this.client = this.server.CreateClient();
        }

        [TestMethod]
        public async Task TestShouldReturnPlansAsync()
        {
            var response = await client.GetAsync("/api/Plans");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();

            dynamic message = JsonConvert.DeserializeObject(responseString);
            Assert.IsTrue(message.Count == 2);

            // Assert.IsTrue(responseString == "[{\"id\":1,\"name\":\"Italy Road Trip\",\"keyWords\":\"Venice, Museums\"},{\"id\":2,\"name\":\"Spindl\",\"keyWords\":\"Walking, Nature\"}]", responseString);
        }
    }
}
