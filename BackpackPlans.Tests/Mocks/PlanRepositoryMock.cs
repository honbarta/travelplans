using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BackpackPlans.Data.Models.Domain;
using BackpackPlans.Data.Repositories;

namespace BackpackPlans.Repositories
{
    public class PlanRepositoryMock : IPlanRepository
    {
        private static List<Plan> plans = new List<Plan>
        {
            // TODO: fix
            // new Plan {Id = 1, Name = "Test", KeyWords = "Test" },
            // new Plan {Id = 2, Name = "Test 2", KeyWords = "Test 2, Test 3" },
        };

        public Task<bool> DeletePlanItem(PlanItem item)
        {
            throw new NotImplementedException();
        }

        public Task<Plan> GetPlan(int id)
        {
            throw new NotImplementedException();
        }

        public Plan GetPlanDetail(int id)
        {
            return plans.Find(p => p.Id == id);
        }

        public ICollection<Plan> GetPlans(int? userId)
        {
            return plans;
        }

        public bool SavePlan(Plan plan)
        {
            plans.Add(plan);
            return true;
        }

        public Task<bool> SavePlanItem(PlanItem planItem)
        {
            throw new NotImplementedException();
        }

        public Task<(Plan plan, User user)> UnlockPlan(int planId, int userId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdatePlan(Plan oldPlan, Plan newPlan)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdatePlanItem(PlanItem oldPlanItem, PlanItem newPlanItem)
        {
            throw new NotImplementedException();
        }
    }
}