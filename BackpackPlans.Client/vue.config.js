module.exports = {
  transpileDependencies: ['vuetify'],
  devServer: {
    proxy: 'https://localhost:5001'
  },
  css: {
    loaderOptions: {
      scss: {
        prependData: `@import "@/styles/_variables.scss";`
      }
    }
  }
};
