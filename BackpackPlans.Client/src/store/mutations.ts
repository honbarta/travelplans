import { ExploreResponseItem } from '@/api/foursquare';
import { Place, State } from '.';

export enum MutationTypes {
  ADD_PLACE = 'ADD_PLACE',
  CHANGE_PLACE = 'CHANGE_PLACE',
  ADD_FOURSQUARE_RECOMMENDATIONS = 'ADD_FOURSQUARE_RECOMMENDATIONS'
}

export const mutations = {
  [MutationTypes.ADD_PLACE](state: State, place: Place) {
    state.places.push(place);
  },
  [MutationTypes.CHANGE_PLACE](state: State, place: Place) {
    const index = state.places.findIndex(p => p.id === place.id);
    state.places[index] = place;
  },
  [MutationTypes.ADD_FOURSQUARE_RECOMMENDATIONS](state: State, payload: { placeId: number; foursquareData: ExploreResponseItem[] }) {
    state;
  }
};

export default mutations;
