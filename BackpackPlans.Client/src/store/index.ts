import { ExploreResponseItem } from '@/api/foursquare';
import Vue from 'vue';
import Vuex from 'vuex';
import actions from './actions';
import { getters } from './getters';
import mutations from './mutations';

Vue.use(Vuex);

export type Place = {
  id: number;
  city: string;
  lat: number;
  lon: number;
  days: number[];
};

export type FoursquareState = {
  [key: number]: ExploreResponseItem; // key is place ID
};

export type State = {
  places: Place[];
  foursquare: FoursquareState;
};

export default new Vuex.Store<State>({
  state: {
    places: [],
    foursquare: {}
  },
  mutations: {
    addPlace(state, place: Place) {
      state.places.push(place);
    },
    changePlace(state, place: Place) {
      const placeIndex = state.places.findIndex(p => p.id === place.id);

      // if I added x days in the middle of my shedule, I need to move all the following places by x days
      // so I need to find out what x is by getting number of days in the place I just edited
      // and subtracting it from the "day index" of the first day in the next location - 1
      if (placeIndex < state.places.length - 1) {
        const daysDiff = place.days.length - (state.places[placeIndex + 1].days[0] - 1);

        if (daysDiff != 0) {
          for (let i = placeIndex + 1; i < state.places.length; i++) {
            state.places[i].days = state.places[i].days.map(day => day + daysDiff);
          }
        }
      }

      state.places[placeIndex] = place;
    }
  },
  getters: {
    getPlaceById: (state: State) => (id: number) => {
      return state.places.find(place => place.id === id);
    },
    getNumberOfDays: (state: State) => () => {
      return state.places.reduce((acc, cur) => acc + cur.days.length, 0);
    }
  },
  actions,
  modules: {} // TODO split app to basic modules. User, Plan... etc.
});
