
import { ActionContext } from 'vuex';
import { State } from '.';
import {getRecommendedPlaces} from '@/api/foursquare'

export enum ActionTypes {
  FETCH_FOURSQUARE = 'FETCH_FOURSQUARE'
}

export const actions = {
  async [ActionTypes.FETCH_FOURSQUARE](context: ActionContext<State, State>, placeName: string) {
    const foursquareExploreData = await getRecommendedPlaces(placeName);
    console.log("foursquareExploreData", foursquareExploreData)
  }
};

export default actions;
