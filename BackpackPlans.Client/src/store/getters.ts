import { State } from '.';

export enum GettersType {
  GET_PLACE_BY_ID = 'GET_PLACE_BY_ID'
}

export const getters = {
  [GettersType.GET_PLACE_BY_ID]: (state: State) => (id: number) => {
    return state.places.find(place => place.id === id);
  }
};
