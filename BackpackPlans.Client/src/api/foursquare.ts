const CLIENT_ID = '3IIG4I5L5RJSGAQGUDJZW50FZGA3GQPTNWTEEIITKBGOAO5C';
const CLIENT_SECRET = 'DXPKIOPFAZZRASJ3LJDCCHDVZNAYNOQ240XI1X5TL23F0E1E';
const API_VERSION = '20200820';

export interface ExploreResponseItem {
  reasons: any;
  venue: {
    id: string;
    name: string;
    contact: {
      phone?: string;
      formattedPhone?: string;
      twitter?: string;
      instagram?: string;
      facebook?: string;
      facebookUsername?: string;
      facebookName?: string;
    };
    location: {
      address: string;
      crossAddress?: string;
      lat: number;
      lng: number;
      postalCode: string;
      cc?: string;
      city: string;
      state: string;
      country: string;
      formattedAddress: string[];
    };
    url?: string;
    rating: number;
    ratingColor: string;
    photos: { count: number; groups: unknown[] };
    tips: unknown[];
  };
}

/**
 * Fetches foursquare explore data
 * @param place Place must be geocodable otherwise foursquare api returns error
 * @returns Foursquare json response @see https://developer.foursquare.com/docs/api-reference/venues/explore/#response-fields
 */
function fetchRecommendations(place: string): Promise<Response> {
  return fetch(`/api/places/foursquare/${place}`);
}

export async function getRecommendedPlaces(place: string): Promise<(ExploreResponseItem[] & unknown) | undefined> {
  try {
    const frsqResponse = await fetchRecommendations(place);
    if (frsqResponse.status !== 200) {
      throw new Error('Foursquare explore ednpoint did not return ok. ' + frsqResponse.statusText);
    }

    const responseData = await frsqResponse.json();
    console.log('responseData', responseData);

    return responseData.response.groups[0].items;
  } catch (e) {
    console.error('getRecommendedPlaces', e);
  }
}
