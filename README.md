
## TODO

-) Remove BackpackPlans.Api
-) Facebook IDs in client in env variables
-) Fix S3 images upload
3) BackpackPlans appsettings based on environment
4) Fix data
  - new lines in plan items
  - images
5) Custom DNS Domain
6) AWS hardening
  - Security Groups
  - Private and public subnets
-) Nat Gateway instance - make reserved
8) Optimise or remove huge gifs from the landing page
9) Ping endpoint to wake up lambda and DB
-) New service for image optimisation
  - SQS + Lambda
  - https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/sqs-examples-send-receive-messages.html

