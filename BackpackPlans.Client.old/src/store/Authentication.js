import { push } from 'react-router-redux'

const authenticatedType = 'FB_AUTHENTICATED';
const logoutType = 'LOGOUT';
const requestBuyBackpacksType = 'REQUEST_BUY_BACKPACKS';
export const updateBackpacksCountType = 'RESPONSE_UPDATE_BACKPACKS_COUNT';
const tourShownType = 'TOUR_SHOWN';

const initialState = { authenticated: false, isBuying: false };

const baseUrl = process.env.REACT_APP_BASE_URL

export const actionCreators = {
  authenticated: (user, path = '/plans') => async (dispatch) => {

    const url = baseUrl + `api/Users/authenticate`;
    const response = await fetch(url, {
      method: 'POST',
      cache: 'no-cache',
      headers: {
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: user.accessToken
      }),
    });
    const authenticatedUser = await response.json();

    dispatch({ type: authenticatedType, user: authenticatedUser });
    dispatch(push(path))
  },
  logout: (callback) => (dispatch) => {
    dispatch({ type: logoutType, callback });
  },
  buyBackpacks: (count) => async (dispatch, getState) => {
    dispatch({ type: requestBuyBackpacksType });

    const token = getState().auth.user.token
    const url = baseUrl + `api/Users/BuyBackpacks`;
    const response = await fetch(url, {
      method: 'POST',
      cache: 'no-cache',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({
        count
      }),
    });
    const countReceived = await response.json();

    dispatch({ type: updateBackpacksCountType, count: countReceived.count });
  },

  tourShown: (name) => (dispatch) => {
    dispatch({ type: tourShownType, name });
  },

  register: (retry = 0) => async (dispatch, getState) => {

    if (retry > 5) {
      return
    }

    const user = getState().auth.user
    const token = user.token
    const url = baseUrl + `api/Users/register`;

    try {
      const response = await fetch(url, {
        method: 'POST',
        cache: 'no-cache',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
          ...user
        }),
      });
    } catch (e) {
      actionCreators.register(retry + 1)(dispatch, getState)
    }
  },
};

export const reducer = (state, action) => {
  state = state || initialState;

  if (action.type === authenticatedType) {
    return {
      ...state,
      authenticated: true,
      user: action.user,
      tour: action.user.firstLogin ? {plans: true, newPlan: true} : null
    };
  }

  if (action.type === logoutType) {
    window.FB.logout(() => {
      action.callback()
    })
    return {
      ...state,
      authenticated: false,
      user: null
    };
  }

  if (action.type === requestBuyBackpacksType) {
    return {
      ...state,
      isBuying: true,
    };
  }

  if (action.type === updateBackpacksCountType) {
    return {
      ...state,
      isBuying: false,
      user: {
        ...state.user,
        backpacksCount: action.count
      }
    };
  }

  if (action.type === tourShownType) {
    return {
      ...state,
      tour: {
        ...state.tour,
        [action.name]: false
      }
    }
  }

  return state;
};
