import { push } from 'react-router-redux'

import { updateBackpacksCountType } from './Authentication';
const requestPlansType = 'REQUEST_PLANS';
const receivePlansType = 'RECEIVE_PLANS';
const requestPlanDetailType = 'REQUEST_PLAN_DETAIL';
const receivePlanDetailType = 'RECEIVE_PLANS_DETAIL';
const addFilterTagsType = 'ADD_FILTER_TAG'
const removeFilterTagsType = 'REMOVE_FILTER_TAG'
const changeFilterTripLengthType = 'CHANGE_FILTER_DURATION'
const requestSaveNewPlanType = 'REQUEST_SAVE_NEW_PLAN'
const receiveSaveNewPlanType = 'RECEIVE_SAVE_NEW_PLAN'
const erasePlanDetailType = 'ERASE_PLAIN_DETAIL'
const requestTagsType = 'REQUEST_TAGS';
const receiveTagsType = 'RECEIVE_TAGS';
const requestSaveNewCommentType = 'REQUEST_SAVE_NEW_COMMENT'
const receiveSaveNewCommentType = 'RECEIVE_SAVE_NEW_COMMENT'
const requestLikeType = 'REQUEST_SAVE_LIKE'
const receiveLikeType = 'RECEIVE_SAVE_LIKE'

const initialState = {
  plans: [],
  isLoading: false,
  selectedTags: [],
  filteredPlans: [],
  tripLength: null,
  planDetail: null,
  isSaving: false,
  isLoadingTags: false,
  isLiking: false,
  tags: []
};

const baseUrl = process.env.REACT_APP_BASE_URL

export const actionCreators = {

  // TODO: add retry to all calls
  requestPlans: (userId, retry = 0) => async (dispatch, getState) => {

    if (retry > 5) {
      dispatch({ type: receivePlansType, plans: [] });
      return
    }

    if (getState().isLoading && retry === 0) {
      // Don't issue a duplicate request (we already have or are loading the requested data)
      return;
    }

    dispatch({ type: requestPlansType });

    const token = getState().auth.user.token
    const url = baseUrl + `api/Plans${userId ? `?userId=${userId}` : ''}`;

    let plans;
    try {
      const response = await fetch(url, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      });
      plans = await response.json();
    } catch(e) {
      actionCreators.requestPlans(userId, retry + 1)(dispatch, getState)
      return
    }

    dispatch({ type: receivePlansType, plans });
  },

  addFilterTag: tag => (dispatch, getState) => {
    dispatch({ type: addFilterTagsType, tag });
  },

  removeFilterTag: index => (dispatch, getState) => {
    dispatch({ type: removeFilterTagsType, index });
  },

  changeFilterTripLength: length => (dispatch) => {
    dispatch({ type: changeFilterTripLengthType, length });
  },

  requestPlanDetail: id => async (dispatch, getState) => {

    if (getState().isLoading) {
      return;
    }

    const token = getState().auth.user.token

    dispatch({ type: requestPlanDetailType });

    const url = baseUrl + `api/Plans/${id}`;
    const response = await fetch(url, {
      headers: {
        'Authorization': `Bearer ${token}`
      },
    });
    const planDetail = await response.json();

    dispatch({ type: receivePlanDetailType, planDetail });
  },

  saveNewPlan: plan => async (dispatch, getState) => {

    if (getState().isSaving) {
      return;
    }

    const token = getState().auth.user.token

    dispatch({ type: requestSaveNewPlanType });

    const url = baseUrl + `api/Plans`;
    const response = await fetch(url, {
      method: 'POST',
      cache: 'no-cache',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(plan), // body data type must match "Content-Type" header
    });
    const newPlan = await response.json();

    dispatch({ type: receivePlanDetailType, newPlan });
    dispatch(push('/plans'))
  },

  updatePlan: plan => async (dispatch, getState) => {

    if (getState().isSaving) {
      return;
    }

    const token = getState().auth.user.token

    dispatch({ type: requestSaveNewPlanType });

    const url = baseUrl + `api/Plans/${plan.id}`;
    const response = await fetch(url, {
      method: 'PUT',
      cache: 'no-cache',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(plan),
    });
    const newPlan = await response.json();

    dispatch({ type: receivePlanDetailType, newPlan });
    dispatch(push(`/plans/${plan.id}`))
  },

  unlockPlan: id => async (dispatch, getState) => {

    if (getState().isLoading) {
      return;
    }

    const token = getState().auth.user.token

    dispatch({ type: requestPlanDetailType });

    const url = baseUrl + `api/Plans/Unlock`;
    const response = await fetch(url, {
      method: 'POST',
      cache: 'no-cache',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({
        planId: id
      }),
    });
    const unlockedPlan = await response.json();

    dispatch({ type: receivePlanDetailType, planDetail: unlockedPlan.plan });
    dispatch({ type: updateBackpacksCountType, count: unlockedPlan.backpackCount });
  },

  createNewPlan: () => (dispatch) => {
    dispatch({ type: erasePlanDetailType });
    dispatch(push('/plans/new'))
  },

  requestTags: (retry = 0) => async (dispatch, getState) => {

    if (retry > 5) {
      dispatch({ type: receiveTagsType, tags: [] });
      return
    }

    if (getState().isLoadingTags && retry === 0) {
      return;
    }

    dispatch({ type: requestTagsType });

    let tags
    try {
      // TODO: request repeats too often, extract a generic method
      const token = getState().auth.user.token
      const url = baseUrl + `api/Plans/tags`;
      const response = await fetch(url, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      });
      tags = await response.json();
    } catch (e) {
      actionCreators.requestTags(retry + 1)(dispatch, getState)
      return
    }


    dispatch({ type: receiveTagsType, tags });
  },

  saveNewComment: (planId, comment) => async (dispatch, getState) => {

    if (getState().isSaving) {
      return;
    }

    const token = getState().auth.user.token

    dispatch({ type: requestSaveNewCommentType });

    const url = baseUrl + `api/Plans/${planId}/comments`;
    const response = await fetch(url, {
      method: 'POST',
      cache: 'no-cache',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(comment)
    });
    const newComment = await response.json();
    newComment.user = getState().auth.user;

    dispatch({ type: receiveSaveNewCommentType, newComment })
  },

  likePlan: (planId) => async (dispatch, getState) => {
    if (getState().isLiking) {
      return;
    }

    const token = getState().auth.user.token

    dispatch({ type: requestLikeType });

    const url = baseUrl + `api/Plans/${planId}/likes`;
    const response = await fetch(url, {
      method: 'POST',
      cache: 'no-cache',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
      }
    });

    dispatch({ type: receiveLikeType })
  }

};

export const reducer = (state, action) => {
  state = state || initialState;

  if (action.type === requestPlansType) {
    return {
      ...state,
      isLoading: true
    };
  }

  if (action.type === receivePlansType) {
    return {
      ...state,
      plans: action.plans,
      filteredPlans: action.plans,
      isLoading: false
    };
  }

  if (action.type === requestPlanDetailType) {
    return {
      ...state,
      isLoading: true
    };
  }

  if (action.type === receivePlanDetailType) {
    return {
      ...state,
      planDetail: action.planDetail,
      isLoading: false
    };
  }

  if (action.type === requestSaveNewPlanType || action.type === requestSaveNewCommentType) {
    return {
      ...state,
      isSaving: true
    };
  }

  if (action.type === receiveSaveNewPlanType) {
    return {
      ...state,
      isSaving: false
    };
  }

  if (action.type === erasePlanDetailType) {
    return {
      ...state,
      planDetail: null
    }
  }

  if (action.type === requestTagsType) {
    return {
      ...state,
      isLoadingTags: true
    };
  }

  if (action.type === receiveTagsType) {
    return {
      ...state,
      tags: action.tags,
      isLoadingTags: false
    };
  }

  if (action.type === receiveSaveNewCommentType) {
    return {
      ...state,
      planDetail: {
        ...state.planDetail,
        comments: [
          action.newComment,
          ...state.planDetail.comments,
        ]
      },
      isSaving: false
    }
  }

  if (action.type === requestLikeType) {
    return {
      ...state,
      isLiking: true
    }
  }

  if (action.type === receiveLikeType) {
    return {
      ...state,
      isLiking: false
    }
  }

  // TODO: definitely add unit tests
  if ([addFilterTagsType, removeFilterTagsType, changeFilterTripLengthType].indexOf(action.type) > -1) {
    let selectedTags = state.selectedTags;
    let tripLength = state.tripLength;

    if (action.type === addFilterTagsType) {
      selectedTags = [...state.selectedTags, action.tag]
    }

    if (action.type === removeFilterTagsType) {
      selectedTags = state.selectedTags.slice(0)
      selectedTags.splice(action.index, 1)
    }

    if (action.type === changeFilterTripLengthType) {
      tripLength = action.length
    }

    return {
      ...state,
      tripLength,
      selectedTags,
      filteredPlans: state.plans
        .filter(plan => new RegExp(selectedTags.map(tag => tag.name).join("|")).test(plan.keyWords.map(tag => tag.name).join()))
        .filter(plan => {
          if (tripLength === null) {
            return true;
          }

          return plan.tripLength >= tripLength.value.lower && plan.tripLength <= tripLength.value.upper
        })
    }
  }

  return state;
};
