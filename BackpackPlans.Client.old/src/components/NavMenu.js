import React from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {actionCreators as plansActionCreators} from '../store/Plans';
import {actionCreators as authActionCreators} from '../store/Authentication';

import './NavMenu.css';

class NavMenu extends React.Component {
  constructor (props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }

  toggle () {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  handleLogout = (e) => {
    e.preventDefault(); 
    this.props.logout(() => this.props.history.push("/"))
  }

  handleNewPlan = () => {
    this.props.createNewPlan()
  }

  render () {
    return (
      <header>
        <Navbar className="navbar navbar-expand-lg navbar-light fixed-top navbar-shrink" light id="mainNav">
          <Container>
            <NavbarBrand tag={Link} to="/">Backpack Plans</NavbarBrand>
            {(this.props.location.pathname !== '/' && this.props.location.pathname !== '/login' ) && 
              <React.Fragment>
                <NavbarToggler onClick={this.toggle} className="mr-2" />
                <Collapse className="d-lg-inline-flex flex-lg-row-reverse" isOpen={this.state.isOpen} navbar>
                  <ul className="navbar-nav flex-grow">
                    <NavItem>
                      <Button color="primary" className="nav-link" onClick={this.handleNewPlan.bind(this)}>Share New Plan</Button>
                    </NavItem>
                    <NavItem>
                      <NavLink tag={Link} className="text-dark" to="/plans">Travel Plans</NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink tag={Link} className="text-dark" to="/user">User Detail</NavLink>
                    </NavItem>
                    <NavItem>
                    <Button color="link" className="nav-link" onClick={this.handleLogout}>Logout</Button>
                    </NavItem>
                  </ul>
                </Collapse>
              </React.Fragment>
            }
            {(this.props.location.pathname === '/') && 
              <NavLink tag={Link} className="btn btn-primary" to="/plans">Login or Sign In</NavLink>
            }
          </Container>
        </Navbar>
      </header>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  dispatch => bindActionCreators({...plansActionCreators, ...authActionCreators}, dispatch)
)(NavMenu);

