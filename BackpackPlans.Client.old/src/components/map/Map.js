import React from 'react';

import './Map.css'

class Map extends React.Component {

  map = null

  state = {
    id: `map${this.props.id ? this.props.id : ''}`
  }

  componentDidMount() {
    const props = this.props
    const state = this.state
    
    const mapOptions = {
      source: 'vector',
      key: 'WKAAQdvZNEccgbHEBGzAovAKewGSba7T',
      basePath: '/tomtom-sdk',
      scrollWheelZoom: false,
    }

    if (props.search) {
      mapOptions.zoomControl = false
    }

    const map = window.tomtom.L.map(state.id, mapOptions);
    this.map = map

    if (props.markers) {

      let anyMarkerSet = false

      props.markers.forEach(marker => {

        if (marker.coordinates[0] === 0 && marker.coordinates[1] === 0) {
          return
        }

        anyMarkerSet = true

        let mapMarker
        if (marker.markerColors && marker.markerColors.font) {
          const svg = `
          <svg viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <circle cx="12" cy="12" r="10" stroke="black" fill="${marker.markerColors.background}" stroke-width="2"/>
            <text x="8" y="16" font-size="14" fill="${marker.markerColors.font}">${marker.markerLabel}</text>
          </svg>`;
          const iconUrl = 'data:image/svg+xml;base64,' + btoa(svg);

          mapMarker = window.tomtom.L.marker(marker.coordinates, {
            icon: window.tomtom.L.icon({
              iconUrl: iconUrl,
              iconSize: [24, 24],
              iconAnchor: [12, 12]
            })
          })
        } else {
          mapMarker = window.tomtom.L.marker(marker.coordinates)
        }

        mapMarker.addTo(map)

        if (marker.label) {
          mapMarker.bindPopup(marker.label);
        }
      })

      if (anyMarkerSet) {
        map.fitBounds(props.markers.filter(marker => marker.coordinates[0] !== 0 || marker.coordinates[1] !== 0).map(marker => marker.coordinates), {maxZoom: 15})
      }
    }

    if (this.props.search) {
      var searchBoxInstance = window.tomtom.searchBox({
        collapsible: false,
        searchOnDragEnd: 'never'
      }).addTo(map);

      const showClientMarkerOnTheMap = (result) => {
        const marker = window.tomtom.L.marker(result.data.position).addTo(map);
        const coordinates = [result.data.position.lat, result.data.position.lon]
        map.flyTo(coordinates, 15)
        this.props.onChange(coordinates)
      }

      searchBoxInstance.on(searchBoxInstance.Events.ResultClicked, showClientMarkerOnTheMap);
    }
  }

  componentWillUnmount() {
    if (this.map) {
      this.map.off();
    }
  }

  render() {
    return <div id={this.state.id} className={`map ${this.props.className}`}></div>
  }
}
export default Map;