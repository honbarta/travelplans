import React from 'react';
import { Row, Col, Card, CardTitle, CardText, Jumbotron, CardDeck, CardImg } from 'reactstrap';

import image1 from './IMG_4456.jpg';
import image2 from './IMG_4867.jpg';
import image3 from './IMG_1051.jpg';
import plansListImage from './plans-list.png'
import planDetailGif from './plan-detail.gif';
import planEditGif from './plan-edit.gif';

export default props => (
  <div>
    <Jumbotron className="bg-light">
      <h1 className="display-3">Welcome to Backpack Plans</h1>
      <p className="lead">Your unique personal travel agency</p>
    </Jumbotron>

    <section id="projects" className="projects-section bg-light">
      <div>

        <div className="row align-items-center no-gutters mb-4 mb-lg-5">
          <div className="col-xl-8 col-lg-7">
            <img className="img-fluid mb-3 mb-lg-0" src={image2} alt="" />
          </div>
          <div className="col-xl-4 col-lg-5">
            <div className="featured-text text-center text-lg-left">
              <h4>Search for perfect vacation plans</h4>
              <p className="text-black-50 mb-0">Are you planning your next trip? Get inspiration for you next vacation by others' vacation plans.</p>
            </div>
          </div>
        </div>

        <div className="row align-items-center no-gutters mb-4 mb-lg-5">
          <div className="col-xl-4 col-lg-5">
            <div className="featured-text text-center text-lg-left">
              <h4>Filter, check, investigate at one place</h4>
              <p className="text-black-50 mb-0">Do you have to go through tens of travel blogs to find more info about your next destination? Have everything at one place, filter by place of interest, country or town and check experience of other travelers.</p>
            </div>
          </div>
          <div className="col-xl-8 col-lg-7">
            <img className="img-fluid mb-3 mb-lg-0" src={plansListImage} alt="" />
          </div>
        </div>

        <div className="row justify-content-center no-gutters">
          <div className="col-lg-6">
            <img className="img-fluid" src={image3} alt="" />
          </div>
          <div className="col-lg-6">
            <div className="bg-black text-center h-100 project">
              <div className="d-flex h-100">
                <div className="project-text w-100 my-auto text-center text-lg-right">
                  <h4 className="text-white">Get inspired</h4>
                  <p className="mb-0 text-white-50">Save time and use experiences of others - enjoy your vacation without painful and long planning, but still unique and personal.</p>
                  <hr className="d-none d-lg-block mb-0 mr-0" />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row justify-content-center no-gutters">
          <div className="col-lg-6">
            <img className="img-fluid" src={planDetailGif} alt="" />
          </div>
          <div className="col-lg-6 order-lg-first">
            <div className="bg-black text-center h-100 project">
              <div className="d-flex h-100">
                <div className="project-text w-100 my-auto text-center text-lg-right">
                  <h4 className="text-white">Have you next vacation planned in a second</h4>
                  <p className="mb-0 text-white-50">Check detailed itinerary, todos before you go, map of all major stops, rought cost...all at one place</p>
                  <hr className="d-none d-lg-block mb-0 mr-0" />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row justify-content-center no-gutters mb-5 mb-lg-0">
          <div className="col-lg-6">
            <img className="img-fluid" src={image1} alt="" />
          </div>
          <div className="col-lg-6">
            <div className="bg-black text-center h-100 project">
              <div className="d-flex h-100">
                <div className="project-text w-100 my-auto text-center text-lg-left">
                  <h4 className="text-white">Share your amazing trips</h4>
                  <p className="mb-0 text-white-50">Share plan you created for your past travels. Help others to enjoy your perfect vacation ideas and reause effort you put into the planning.</p>
                  <hr className="d-none d-lg-block mb-0 ml-0" />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row justify-content-center no-gutters">
          <div className="col-lg-6">
            <img className="img-fluid" src={planEditGif} alt="" />
          </div>
          <div className="col-lg-6 order-lg-first">
            <div className="bg-black text-center h-100 project">
              <div className="d-flex h-100">
                <div className="project-text w-100 my-auto text-center text-lg-right">
                  <h4 className="text-white">Share and help others</h4>
                  <p className="mb-0 text-white-50">Save and share your previous trips so others can get inspired, safe time and enjoy your trip the same weay you did.</p>
                  <hr className="d-none d-lg-block mb-0 mr-0" />
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section>

    <section id="signup" className="signup-section">
      <div className="container">
        <div className="row">
          <div className="col-md-10 col-lg-8 mx-auto text-center">

            <i className="far fa-paper-plane fa-2x mb-2 text-white"></i>
            <h2 className="text-white mb-5">Subscribe to receive updates!</h2>

            <form className="form-inline d-flex">
              <input type="email" className="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" id="inputEmail" placeholder="Enter email address..." />
              <button type="submit" className="btn btn-primary mx-auto">Subscribe</button>
            </form>

          </div>
        </div>
      </div>
    </section>

    <footer className="bg-black small text-center text-white-50">
      <div className="container">
        Copyright © Backpack Plans 2019
      </div>
    </footer>

  </div>
);
