import React from 'react';
import { Container } from 'reactstrap';
import NavMenu from './NavMenu';

export default props => (
  <div>
    <NavMenu location={props.location}  history={props.history} />
    <Container id="mainContainer">
      {props.children}
    </Container>
  </div>
);
