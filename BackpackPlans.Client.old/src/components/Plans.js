import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Row, Col, FormGroup, Label, Spinner, Button } from 'reactstrap';
import ReactTags from 'react-tag-autocomplete'
import Select from 'react-select'

import './Tags.css'
import './Plans.css'
import * as ThumbUp from 'open-iconic/svg/thumb-up.svg';
import * as Book from 'open-iconic/svg/book.svg';
import * as Image from 'open-iconic/svg/image.svg';

import { actionCreators as planActionCreators } from '../store/Plans';
import { actionCreators as userActionCreators } from '../store/Authentication';
import Loading from './Loading';
import Help from './Help';

class Plans extends Component {

  componentDidMount() {
    this.ensureDataFetched();
  }

  ensureDataFetched() {
    this.props.requestPlans();
    this.props.requestTags();

    if (this.props.user.firstLogin) {
      this.props.register()
    }
  }

  onDelete (i) {
    this.props.removeFilterTag(i)
  }

  onAddition (tag) {
    this.props.addFilterTag(tag)
  }

  options = [
    { value: {lower: 1, upper: 3}, label: '1-3 days' },
    { value: {lower: 4, upper: 7}, label: '4-7 days' },
    { value: {lower: 8, upper: 9999}, label: '7+ days' }
  ]

  handleTripLengthChange = selectedOption => {
    this.props.changeFilterTripLength(selectedOption)
  };

  tourSteps = [
    {
      selector: '',
      content: () => (
        <div>
          <p>Welcome to our portal where you can search for plans for your next trip or share your last vacation.</p>
          <p>Let us guide you really quickly through so you can move around with ease</p>
        </div>
      )
    },
    {
      selector: '.react-tags',
      content: 'Start by specifiing what trips are you looking for. Maybe a specific country? City? Place?',
    }, {
      selector: '.trip-length',
      content: 'Next pick how long do you want to travel. Is it a weekend or full vacation?',
    }, {
      selector: '.table.table-striped',
      content: 'Finally check trips witch match your criteria',
    }, {
      selector: '.nav-item .btn-primary',
      content: 'Next you can start sharing your own travel plans and became famous. Or just have a good feeling for helping others :)',
    }
  ]

  render() {
    return (
      <React.Fragment>
        <Row>
          <Col sm="3">
            <h2>Filters</h2>
            <FormGroup>
              <Label for="trip-length">Tags</Label>
              <ReactTags
                minQueryLength="0"
                tags={this.props.selectedTags}
                suggestions={this.props.tags}
                onDelete={this.onDelete.bind(this)}
                onAddition={this.onAddition.bind(this)}
                placeholderText="Filter by keyword" />
            </FormGroup>
            <FormGroup>
              <Label for="trip-length">Trip Length</Label>
              <Select
                options={this.options}
                value={this.props.tripLength}
                onChange={this.handleTripLengthChange}
                isClearable={true}
                className="trip-length"
                placeholder="Filter by trip length"
                theme={theme => ({
                  ...theme,
                  borderRadius: 5,
                  colors: {
                    ...theme.colors,
                    primary25: '#cfe2e1',
                    primary50: '#b0cfcd',
                    primary75: '#90bcb9',
                    primary: '#64a19d',
                  },
                })}
              />
            </FormGroup>
          </Col>
          <Col sm="9">
            <h1>Plans</h1>
            {renderPlansTable(this.props)}
          </Col>
        </Row>

        <Help tourSteps={this.tourSteps} tourName="plans" />
      </React.Fragment>
    );
  }
}

// TODO: move to a separate component
function renderPlansTable(props) {
  return (
    <Loading isLoading={props.isLoading}>
      <table className='table table-striped'>
        <tbody>
          {props.filteredPlans.sort((plan1, plan2) => plan2.likes.length - plan1.likes.length).map(plan =>
            <tr key={plan.id}>
              <td>
                <p>{plan.user.firstName} {plan.user.lastName}</p>
                <p><img src={plan.user.icon} className="plans_user-icon" alt={`${plan.user.firstName} ${plan.user.lastName}`} /></p>
              </td>
              <td>
                <Link to={`/plans/${plan.id}`}>
                  <h3>{plan.name} for {plan.tripLength} day{plan.tripLength === 1 ? '' : 's'}</h3>
                  <p>{plan.keyWords.map(tag => tag.name).join(', ')}</p>
                  <div className="plan-icons">
                    <span><img src={ThumbUp} className="icon" /> {plan.likes.length}</span>
                    <span><img src={Image} className="icon" /> {plan.imagesCount}</span>
                    <span><img src={Book} className="icon" /> {Math.ceil(plan.wordsCount / 200)} mins read</span>
                  </div>
                </Link>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </Loading>
  );
}

const mapStateToProps = (state) => {
  return {
    plans: state.plans.plans,
    selectedTags: state.plans.selectedTags,
    filteredPlans: state.plans.filteredPlans,
    tripLength: state.plans.tripLength,
    isLoading: state.plans.isLoading,
    tags: state.plans.tags,
    isLoadingTags: state.plans.isLoadingTags,
    user: state.auth.user
   };
};

export default connect(
  mapStateToProps,
  dispatch => bindActionCreators({...planActionCreators, ...userActionCreators}, dispatch)
)(Plans);
