import React from 'react';
import Tour from 'reactour'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { actionCreators } from '../store/Authentication';

import * as QuestionMark from 'open-iconic/svg/question-mark.svg';

class Help extends React.Component {

  state = {
    isTourOpen: this.props.tour && this.props.tour[this.props.tourName]
  }
  
  closeTour = () => {
    this.setState({isTourOpen: false})
  }

  showHelp = () => {
    this.setState({isTourOpen: true})
  }

  onOpen = () => {
    this.props.tourShown(this.props.tourName)
  }

  render () {
    return (
      <React.Fragment>
        <Tour
          steps={this.props.tourSteps}
          isOpen={this.state.isTourOpen}
          onRequestClose={this.closeTour}
          accentColor="#64a19d"
          scrollDuration="500"
          onAfterOpen={this.onOpen}
          rounded={5} />

        <div onClick={this.showHelp} className="help">
          <div className="help__text">Help</div><div className="help__icon"><img src={QuestionMark} /></div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  tour: state.auth.tour
});

export default connect(
  mapStateToProps,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(Help);

