import React, { Component } from 'react';
import { Alert } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from "react-router";
import FacebookLogin from 'react-facebook-login';

import { actionCreators } from '../store/Authentication';

class Login extends Component {

  responseFacebook = (response) => {
    const path = this.props.location.state && this.props.location.state.from ? this.props.location.state.from.pathname : '/plans'
    this.props.authenticated(response, path)
  }

  componentDidMount() {
    // warm start of DB
    const baseUrl = process.env.REACT_APP_BASE_URL
    const url = baseUrl + 'api/ping/db';
    fetch(url)
  }

  render() {
    return (
      <React.Fragment>
        <h1>Login</h1>
        <Alert color="success">
          We only ask Facebook for three public information about you:
          <ul>
            <li>Your name - so we can call you by name</li>
            <li>Thumbnail of your picture - so we can show others who is writing so amazing vacation plans</li>
            <li>Your email address - so we can identify you in our system</li>
          </ul>
        </Alert>
        <FacebookLogin
          appId={process.env.REACT_APP_FB_APP_ID}
          autoLoad={true}
          fields="name,email,picture"
          cookie={false}
          isMobile={false}
          disableMobileRedirect={true}
          callback={this.responseFacebook.bind(this)} />
      </React.Fragment>

    );
  }
}

const mapStateToProps = () => ({});

export default withRouter(connect(
  mapStateToProps,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(Login));
