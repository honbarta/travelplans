import React from 'react';
import { Row, Col, FormGroup, Label, Input } from 'reactstrap';
import SimpleMDE from "react-simplemde-editor";
import "easymde/dist/easymde.min.css";

const baseUrl = process.env.REACT_APP_BASE_URL

class MdInput extends React.Component {

  componentDidMount() {
    if (!window.MdInputImageUploadSet) { // Hack until https://github.com/Ionaru/easy-markdown-editor/pull/109 is released
      window.MdInputImageUploadSet = true
      const props = this.props

      const o = XMLHttpRequest.prototype.open;
      XMLHttpRequest.prototype.open = function(){
        const res = o.apply(this, arguments);
        const err = new Error();
        this.setRequestHeader('Authorization', `Bearer ${props.token}`);
        return res;
      }
    }
  }

  render() {

    const props = this.props
    let formControl = "form-control";

    if (props.touched && !props.valid) {
        formControl = 'form-control control-error';
    }

    return (
        <Row className={props.name}>
          <Label for={props.name} sm={2}>{props.validationRules && props.validationRules.isRequired && '*'}{props.label}</Label>
          <Col sm={10}>
            <SimpleMDE
              {...props}
              name={props.name}
              id={props.name}
              placeholder={props.placeholder}
              onChange={props.onChange}
              options={{
              onToggleFullScreen: props.onToggleFullScreen,
              imageUploadEndpoint: baseUrl + 'api/s3/upload-image',
              // showIcons: ['upload-image'],
              uploadImage: true, // TODO: uploading images probably does not refresh state because unless user makes a changes of text after the upload, images got lost
              ...props.options
            }} />
          </Col>
        </Row>
    );

  }
}

export default MdInput;