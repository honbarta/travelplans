import React from 'react';
import { Row, Col, FormGroup, Label, Input } from 'reactstrap';

const TextInput = props => {

    let formControl = "form-control";

    if (props.touched && !props.valid) {
        formControl = 'form-control control-error';
    }

    return (
        <FormGroup row>
          <Label for={props.name} sm={2}>{props.validationRules && props.validationRules.isRequired && '*'}{props.label}</Label>
          <Col sm={10}>
            <Input type="text" name={props.name} id={props.name} className={formControl} {...props} />
          </Col>
        </FormGroup>
    );
}

export default TextInput;