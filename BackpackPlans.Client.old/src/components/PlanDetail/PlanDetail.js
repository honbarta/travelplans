import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Row, Col, ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText, Jumbotron, Button, Alert, Spinner } from 'reactstrap';
import { Link } from 'react-router-dom';
import ReactMarkdown from 'react-markdown'

import TextInput from '../form/TextInput';
import UnlockPlanButton from './UnlockPlanButton'

import * as ThumbUp from 'open-iconic/svg/thumb-up.svg';
import * as Bullhorn from 'open-iconic/svg/bullhorn.svg';

import { actionCreators } from '../../store/Plans';

import './PlanDetail.css'
import Map from '../map/Map';
import Loading from '../Loading';

class PlanDetail extends Component {

  state = {
    showNewComment: false,
    newComment: {
      value: '',
      placeholder: 'What do you want to ask?'
    },
    unlockPopoverOpen: false,
    hasBeenLiked: false
  }

  commentsRef = React.createRef();

  componentDidMount() {
    this.ensureDataFetched();
  }

  ensureDataFetched() {
    const planId = this.props.match.params.id
    this.props.requestPlanDetail(planId)
  }

  handlePaid() {
    const planId = this.props.match.params.id
    this.props.unlockPlan(planId);
  }

  handleAskAuthor() {
    this.commentsRef.current.scrollIntoView({ behavior: 'smooth', block: 'start' })
  }

  handleNewComment() {
    this.setState({
      showNewComment: !this.state.showNewComment,
      newComment: {
        placeholder: this.state.newComment.placeholder,
        value: ''
      }
    })
  }

  handleNewCommentChange = event => {
    const value = event.target.value;

    this.setState({
      newComment: {
        placeholder: this.state.newComment.placeholder,
        value
      }
    });
  }

  saveNewComment() {
    var newComment = {
      text: this.state.newComment.value
    }

    const planId = this.props.match.params.id
    this.props.saveNewComment(planId, newComment)

    this.setState({
      showNewComment: false,
      newComment: {...this.state.newComment, value: ''}})
  }

  handleClickOutside() {
    this.handleDisabledUnlock()
  }

  handleDisabledUnlock = () => {
    this.setState({unlockPopoverOpen: !this.state.unlockPopoverOpen})
  }

  handleLike = () => {
    this.setState({hasBeenLiked: true})
    const planId = this.props.match.params.id
    this.props.likePlan(planId)
  }

  getPlanItemTypeDescription = (type) => type === 1 ? 'Day' : 'Night'

  render() {

    const plan = this.props.planDetail
    const planId = this.props.match.params.id

    return (
      <Loading isLoading={this.props.isLoading || !plan}>
        {plan &&
        <Row>
          <Col sm="3">
            <h2>{plan.user.firstName} {plan.user.lastName}</h2>
            <img src={plan.user.icon} className="plans_user-icon" alt={`${plan.user.firstName} ${plan.user.lastName}`} />
          </Col>
          <Col sm="9">
            <Jumbotron>
              <h1 className="display-3">{plan.name}</h1>
              <p className="lead"><ReactMarkdown source={plan.description} /></p>
              <hr className="my-2" />
              <p>{plan.keyWords.map(tag => tag.name).join(', ')}</p>
              <p className="lead plan_detail-action_buttons">
                { (!plan.unlockedPlans || plan.unlockedPlans.length <= 0) &&
                  <UnlockPlanButton bacpackCount={this.props.bacpackCount} handlePaid={this.handlePaid.bind(this)} />
                }
                { this.props.auth.user.id === plan.user.id &&
                  <Link to={`/plans/${planId}/edit`} className="btn btn-secondary">Edit</Link>
                }
                <Button
                  color="secondary"
                  disabled={plan.likes.find(l => l.userId === this.props.auth.user.id) || this.state.hasBeenLiked || this.props.isLiking}
                  onClick={this.handleLike}>
                    {this.props.isLiking && <Spinner type="grow" color="dark" />}
                    <img src={ThumbUp} className="icon" /> Like
                </Button>
                <Button color="secondary" onClick={this.handleAskAuthor.bind(this)}>Ask author</Button>
              </p>
            </Jumbotron>
            <div className={(!plan.unlockedPlans || plan.unlockedPlans.length <= 0) ? 'not-paid' : 'paid'}>
              <ListGroup flush>
                {plan.items.map((item, index) =>
                  <ListGroupItem className={`plan-item ${this.getPlanItemTypeDescription(item.type).toLowerCase()}`}>
                    <Row>
                      <Col className="plan-bullet">
                        <svg viewBox="0 0 30 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="15" cy="50" r="10" stroke="black" fill={item.type === 2 ? 'black' : 'white'} stroke-width="2"/>
                          <text x="11" y="55" font-size="14" fill={item.type === 2 ? 'white' : 'black'}>{item.day}</text>
                          {index < plan.items.length - 1 && <line x1="15" x2="15" y1="60" y2="100" stroke="black" stroke-width="2"/>}
                          {index > 0 && <line x1="15" x2="15" y1="0" y2="40" stroke="black" stroke-width="2"/>}
                        </svg>
                      </Col>
                      <Col>
                        <ListGroupItemHeading>{this.getPlanItemTypeDescription(item.type)} {item.day}</ListGroupItemHeading>
                        <ListGroupItemText>
                          <ReactMarkdown source={item.description} />
                        </ListGroupItemText>
                      </Col>
                    </Row>
                  </ListGroupItem>
                )}
              </ListGroup>
              { plan.items && plan.items.length > 0 && <Map markers={plan.items.map(item => ({
                coordinates: [item.lat, item.lon],
                label: `${this.getPlanItemTypeDescription(item.type)} ${item.day}`,
                markerLabel: item.day,
              markerColors: item.type === 2 ? { background: 'black', font: 'white' } : { background: 'white', font: 'black' } }))} /> }

              <hr />

              <div className="clearfix">
                <h3 ref={this.commentsRef} className="float-left">Comments</h3>
                <Button color="secondary" className="float-right" onClick={this.handleNewComment.bind(this)}>{this.state.showNewComment ? 'Cancel' : 'Create new Comment'}</Button>
              </div>

              {this.state.showNewComment &&
                <div>
                  <TextInput
                    label="New Comment"
                    name="newComment"
                    onChange={this.handleNewCommentChange}
                    {...this.state.newComment}
                  />
                  <Button color="primary" disabled={this.props.isSaving} onClick={this.saveNewComment.bind(this)}>Save new Comment</Button>
                </div>
              }

              <ListGroup flush>
                {plan.comments && plan.comments.length > 0 && plan.comments.map((comment) =>
                  <ListGroupItem key={comment.id}>
                    <ListGroupItemHeading>{comment.user.firstName} {comment.user.lastName}</ListGroupItemHeading>
                    <ListGroupItemText>
                      {comment.text}
                    </ListGroupItemText>
                  </ListGroupItem>
                )}
                {(!plan.comments || plan.comments.length <= 0) &&
                  <Alert color="secondary">
                    No comments
                  </Alert>
                }
              </ListGroup>

              {/* <Button color="danger"><img src={Bullhorn} className="icon" /> Report wrong or misleading plan</Button> */}
            </div>
          </Col>
        </Row>
        }
      </Loading>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    planDetail: state.plans.planDetail,
    isLoading:  state.plans.isLoading,
    bacpackCount: state.auth.user.backpacksCount,
    auth: state.auth,
    isSaving: state.plans.isSaving,
    isLiking: state.plans.isLiking
  };
};

export default connect(
  mapStateToProps,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(PlanDetail);
