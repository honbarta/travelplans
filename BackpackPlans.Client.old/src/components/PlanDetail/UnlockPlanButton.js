import React, { Component } from 'react';
import { Button, Popover, PopoverHeader, PopoverBody } from 'reactstrap';
import onClickOutside from "react-onclickoutside";

import * as Unlock from 'open-iconic/svg/lock-unlocked.svg';

class UnlockPlanButton extends Component {

  state = {
    unlockPopoverOpen: false
  }

  handleClickOutside() {
    this.setState({ unlockPopoverOpen: false })
  }

  handleDisabledUnlock = () => {
    this.setState({ unlockPopoverOpen: !this.state.unlockPopoverOpen })
  }

  render() {
    return (
      <React.Fragment>
        <span id="unlockButton" onClick={this.handleDisabledUnlock}>
          <Button color="primary" disabled={this.props.bacpackCount <= 0} onClick={this.props.handlePaid}>
            <img src={Unlock} className="icon" />
            <span>Unlock with one Backpack</span>
          </Button>
        </span>
        <Popover placement="bottom" target="unlockButton" isOpen={this.state.unlockPopoverOpen}>
          <PopoverHeader>Not enough Backpacks</PopoverHeader>
          <PopoverBody>Write new plan to get more Backpacks</PopoverBody>
        </Popover>
      </React.Fragment>
    )
  }
}

export default onClickOutside(UnlockPlanButton);
