import React from 'react';
import { Spinner } from 'reactstrap';

export default ({isLoading, children}) => (
  <React.Fragment>
    { isLoading && <Spinner style={{ width: '3rem', height: '3rem' }} type="grow" /> }
    { !isLoading && children}
  </React.Fragment>
)
