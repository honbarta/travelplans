import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Row, Col, ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText, Jumbotron, Button, FormGroup, Label } from 'reactstrap';
import ReactTags from 'react-tag-autocomplete'
import Sticky from 'react-sticky-el'

import TextInput from './form/TextInput';
import validate from './form/validate';
import MdInput from './form/MdInput'

import { actionCreators } from '../store/Plans';
import Map from './map/Map';
import Help from './Help';

import './NewPlan.css'


class NewPlan extends Component {

  state = {
    formControls: {
      tripName: {
        value: this.props.planDetail ? this.props.planDetail.name : '',
        placeholder: 'What is the name of your trip',
        valid: this.props.planDetail ? true : false,
        touched: false,
        validationRules: {
          minLength: 5,
          isRequired: true
        }
      },
      tripDescription: {
        value: this.props.planDetail ? this.props.planDetail.description : '',
        placeholder: 'What is the your trip description'
      }
    },
    planItems: this.props.planDetail ? this.props.planDetail.items : [],
    editorFullScreen: false,
    tags: this.props.planDetail ? this.props.planDetail.keyWords : []
  }

  componentDidMount() {
    this.ensureDataFetched();
  }

  ensureDataFetched() {
    this.props.requestTags();
  }


  changeHandler = event => {
    const name = event.target.name;
    const value = event.target.value;

    const updatedControls = {
      ...this.state.formControls
    };
    const updatedFormElement = {
      ...updatedControls[name]
    };
    updatedFormElement.value = value;
    updatedFormElement.touched = true;
    updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

    updatedControls[name] = updatedFormElement;

    this.setState({
      formControls: updatedControls
    });
  }

  changeHandlerMd = (value, name) => {
    this.changeHandler({
      target: {
        name, value
      }
    })
  }

  getPlanItemTypeDescription = (type) => type === 1 ? 'Day' : 'Night'

  formSubmitHandler = () => {

    const plan = {
      name: this.state.formControls.tripName.value,
      description: this.state.formControls.tripDescription.value,
      keyWords: this.state.tags,
      items: this.state.planItems.map(item => ({
        id: item.id ? item.id : 0,
        description: item.description,
        lat: item.map ? item.map[0] : item.lat,
        lon: item.map ? item.map[1] : item.lon,
        type: item.type,
        day: item.day
      }))
    }
    
    if (this.props.match && this.props.match.params.id) {
      const planId = this.props.match.params.id
      plan.id = planId

      this.props.updatePlan(plan)
    } else {
      this.props.saveNewPlan(plan)
    }
    
  }

  handleAddPlanItem = () => {
    let newItem;

    if (this.state.planItems.length <= 0) {
      newItem = {type: 1, day: 1, description: '', map: []}
    } else {
      newItem = {description: '', map: []}
      const lastItem = this.state.planItems[this.state.planItems.length - 1]
      newItem.type = lastItem.type === 2 ? 1 : 2
      newItem.day = lastItem.type === 2 ? lastItem.day + 1 : lastItem.day
    }

    this.setState({planItems: [...this.state.planItems, newItem]})
  }

  handlePlanItemChange = (item, value, type) => {
    item[type] = value
    this.setState({planItems: [...this.state.planItems]})
  }

  handleEditorFullScreen = (fullScreen) => {
    this.setState({editorFullScreen: fullScreen})
  }

  addTag = (tag) => {
    const tags = [].concat(this.state.tags, tag)
    this.setState({ tags })
  }

  removeTag = (i) => {
    const tags = this.state.tags.slice(0)
    tags.splice(i, 1)
    this.setState({ tags })
  }

  addDefaultPlanItem = () => {
    this.setState({planItems: [{type: 1, day: 1, description: '', map: []}]})
  }

  tourSteps = [
    {
      selector: '.new-plan__basic-trip-overview',
      content: 'When creating a new plan, start by filling its name and some keywords characterising it',
    }, {
      selector: '.tripDescription',
      content: 'Next you can write a short description.',
    }, {
      selector: 'button.heading-2.header-2',
      content: 'You can use headings ...',
    }, {
      selector: 'button.bold',
      content: '... bold or italic ...',
    }, {
      selector: 'button.upload-image',
      content: '... or even upload images.',
    }, {
      selector: '.new-plan__add-itinerary-day',
      content: 'Next thing to do is to create detailed itinerary. You can add one or more days (and nigths)',
      action: () => {
        if (this.state.planItems.length === 0) {
          this.addDefaultPlanItem()
        }
      }
    }, {
      selector: '.day-1-description',
      content: 'Fill description of your first day. What did you visit? What have you seen? How was it?',
    }, {
      selector: '.leaflet-control-search-input-container',
      content: 'Last but not least, mark place where were you the most during the day. Maybe it was a landmark, city or county.',
    }
  ]

  render() {
    return (
      <React.Fragment>

        <h3>Trip overview</h3>

        <div className="new-plan__basic-trip-overview">
          <TextInput
            label="Trip Name"
            name="tripName"
            onChange={this.changeHandler}
            {...this.state.formControls.tripName}
          />

          <FormGroup row>
            <Label sm={2}>Keywords</Label>
            <Col sm={10}>
              <ReactTags
                allowNew
                tags={this.state.tags}
                suggestions={this.props.tags}
                onDelete={this.removeTag}
                onAddition={this.addTag}
                placeholderText="Major keywords describing your trip" />
            </Col>
          </FormGroup>
        </div>
        
        <MdInput
          label="Trip Description"
          name="tripDescription"
          onChange={(value) => this.changeHandlerMd(value, 'tripDescription')}
          onToggleFullScreen={this.handleEditorFullScreen}
          token={this.props.userToken}
          options={{
            toolbar: ['heading-2', 'heading-3', 'bold', 'italic', 'link', 'upload-image', 'unordered-list', 'ordered-list', '|', 'fullscreen', 'side-by-side', 'preview']
          }}
          {...this.state.formControls.tripDescription}
        />

        <Row>
          <Col><h3 className="float-left">Trip itinerary</h3></Col>
          <Col>
            <Sticky topOffset={-75} hideOnBoundaryHit={false} stickyStyle={{ transform: 'translateY(77px)', zIndex: 999 }}>
              <Button color="secondary" onClick={this.handleAddPlanItem} className="float-right new-plan__add-itinerary-day">Add Itinerary Day</Button>
            </Sticky>
          </Col>
        </Row>

        <ListGroup flush>
          {this.state.planItems.map((item, index) => 
            <ListGroupItem className={`plan-item ${this.getPlanItemTypeDescription(item.type).toLowerCase()}`} key={index}>
              <Row>
                <Col>
                  <ListGroupItemHeading>{this.getPlanItemTypeDescription(item.type)} {item.day}</ListGroupItemHeading>
                  <ListGroupItemText tag="div">
                    <MdInput
                      label={`${this.getPlanItemTypeDescription(item.type)} ${item.day} Description`}
                      name={`${this.getPlanItemTypeDescription(item.type)}-${item.day}-description`.toLowerCase()}
                      onChange={(value) => this.handlePlanItemChange(item, value, 'description')}
                      value={this.state.planItems[index].description}
                      onToggleFullScreen={this.handleEditorFullScreen}
                      token={this.props.userToken}
                      options={{
                        toolbar: ['bold', 'italic', 'link', 'upload-image', 'unordered-list', 'ordered-list', '|', 'fullscreen', 'side-by-side', 'preview']
                      }}
                      {...this.state.planItems[index]}
                    />
                    <Map 
                      search 
                      className={this.state.editorFullScreen ? 'hidden' : ''}
                      onChange={(coordinates) => this.handlePlanItemChange(item, coordinates, 'map')}
                      id={index}
                      markers={this.state.planItems[index].lat ? [{coordinates: [this.state.planItems[index].lat, this.state.planItems[index].lon]}] : null} />
                  </ListGroupItemText>
                </Col>
              </Row>
            </ListGroupItem>
          )}
        </ListGroup>

        <hr />

        <Button color="primary" onClick={this.formSubmitHandler}
          disabled={!this.state.formControls.tripName.valid}>
          {this.props.planDetail && this.props.planDetail.name ? "Save Edited Plan" : "Save New Plan"}
        </Button>

        <Help tourSteps={this.tourSteps} tourName="newPlan" />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    planDetail: state.plans.planDetail,
    tags: state.plans.tags,
    userToken: state.auth.user.token,
    user: state.auth.user
  };
};

export default connect(
  mapStateToProps,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(NewPlan);
