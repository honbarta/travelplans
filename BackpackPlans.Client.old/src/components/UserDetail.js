import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Row, Col, Jumbotron, Button, Nav, NavItem, NavLink, TabContent, TabPane, Table, Modal, ModalHeader, ModalFooter, ModalBody, FormGroup, Label, Input } from 'reactstrap';
import { Link } from 'react-router-dom';
import ReactMarkdown from 'react-markdown'

import {actionCreators as plansActionCreators} from '../store/Plans';
import {actionCreators as authActionCreators} from '../store/Authentication';

class UserDetail extends Component {

  state = {
    activeTab: '1',
    modal: false,
    backpacksCount: ''
  }

  componentDidMount() {
    this.ensureDataFetched();
  }

  ensureDataFetched() {
    // TODO: refresh user details
    this.props.requestPlans(this.props.auth.user.id);
  }

  toggle = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  toggleModal = () => {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  handleCountChange = (e) => {
    this.setState({backpacksCount: e.target.value})
  }

  buyBackpacks = (e) => {
    e.preventDefault()
    this.props.buyBackpacks(this.state.backpacksCount)
  }

  handleNewPlan = () => {
    this.props.createNewPlan()
  }

  render() {
    return (
      <div>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={`${this.state.activeTab === '1' ? 'active' : ''}`}
              onClick={() => { this.toggle('1'); }}
            >
              My Details
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={`${this.state.activeTab === '2' ? 'active' : ''}`}
              onClick={() => { this.toggle('2'); }}
            >
              My Plans
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Row>
              <Col sm="3">
                <p>{this.props.auth.user.firstName} {this.props.auth.user.lastName}</p>
                <p><img src={this.props.auth.user.icon} className="plans_user-icon" /></p>
                <p>BackPacks: {this.props.auth.user.backpacksCount}</p>
                <p>Plans: {this.props.plans ? this.props.plans.length : '0'}</p>
              </Col>
              <Col sm="9">
                <Jumbotron>
                  <h1 className="display-3">Hello, {this.props.auth.user.firstName} {this.props.auth.user.lastName}!</h1>
                  {this.props.auth.user.backpacksCount <= 0 &&
                    <React.Fragment>
                      <p className="lead">You don't have any BackPacks left.</p>
                      <hr className="my-2" />
                      <p>Write more Plans to get some</p>
                    </React.Fragment>
                  }
                  {this.props.auth.user.backpacksCount > 0 &&
                    <React.Fragment>
                      <p className="lead">What will be your next trip?</p>
                      <hr className="my-2" />
                      <p>Check others plans or create one of your own</p>
                    </React.Fragment>
                  }
                  <p className="lead">
                    <Button color="primary" onClick={this.handleNewPlan.bind(this)}>Share New Plan</Button>
                    {/* {' '}
                    <Button color="secondary" onClick={this.toggleModal}>Buy BackPacks</Button>
                    <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                      <ModalHeader toggle={this.toggleModal}>Buy Backpacks</ModalHeader>
                      <ModalBody>
                      <FormGroup>
                        <Label for="backpacksBount">Number of Backpacks</Label>
                        <Input type="number" name="backpacksBount" id="backpacksBount" placeholder="5" value={this.state.backpacksCount} onChange={this.handleCountChange} />
                      </FormGroup>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.buyBackpacks.bind(this)} disabled={this.props.auth.isBuying}>Pay</Button>{' '}
                        <Button color="secondary" onClick={this.toggleModal}>Cancel</Button>
                      </ModalFooter>
                    </Modal> */}
                  </p>
                </Jumbotron>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="2">
            <Row>
              <Col>
                <Table striped hover>
                  <thead>
                    <tr>
                      <th>Plan Name</th>
                      <th>Duration</th>
                      <th>Description</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.props.plans.map(plan => (
                      <tr key={plan.id}>
                        <td><Link to={`/plans/${plan.id}`}>{plan.name}</Link></td>
                        <td>{plan.tripLength} day{plan.tripLength === 1 ? '' : 's'}</td>
                        <td><ReactMarkdown source={plan.description} /></td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </Col>
            </Row>
          </TabPane>
        </TabContent>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  plans: state.plans.plans
});

export default connect(
  mapStateToProps,
  dispatch => bindActionCreators({...plansActionCreators, ...authActionCreators}, dispatch)
)(UserDetail);
