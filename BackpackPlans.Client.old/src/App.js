import React from 'react';
import { Route, Redirect, Switch } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from './store/Authentication';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';

import Layout from './components/Layout';
import Plans from './components/Plans';
import PlanDetail from './components/PlanDetail/PlanDetail';
import UserDetail from './components/UserDetail';
import NewPlan from './components/NewPlan';
import Login from './components/Login';
import Home from './components/Home/Home';

// TODO: move to a separate component
const Breadcrumbs = (props) => (
  <React.Fragment>
    {props.location.pathname !== '/' &&props.location.pathname !== '/login' && <Breadcrumb>
      <Route path='/:path' component={BreadcrumbsItem} />
    </Breadcrumb>}
  </React.Fragment>
)

const BreadcrumbsItem = ({ match, ...rest }) => (
  <React.Fragment>
    <BreadcrumbItem active={match.isExact ? true : false}>
        <Link to={match.url || ''}>
          {(routes.find(route => {
            if (route.breadcrumbPath) {
              return route.breadcrumbPath === match.path && (route.breadcrumbParams ? match.params.path === route.breadcrumbParams : true)
            }
            return route.path === match.path
          }) || {}).sidebar}
        </Link>
    </BreadcrumbItem>
    <Route path={`${match.url}/:path`} component={BreadcrumbsItem} />
  </React.Fragment>
)

const routes = [
  {
    path: "/plans",
    breadcrumbPath: "/:path",
    breadcrumbParams: "plans",
    exact: true,
    sidebar: <span>Plans</span>,
    main: Plans
  },
  {
    path: "/plans/new",
    breadcrumbPath: "/plans/:path",
    breadcrumbParams: "new",
    exact: true,
    sidebar: <span>New Plan</span>,
    main: NewPlan
  },
  {
    path: "/plans/:id",
    breadcrumbPath: "/plans/:path",
    exact: true,
    sidebar: <span>Plan Detail</span>,
    main: PlanDetail
  },
  {
    path: "/plans/:id/edit",
    breadcrumbPath: "/plans/1/:path", // TODO: does not work
    exact: true,
    sidebar: <span>Edit Plan</span>,
    main: NewPlan
  },
  {
    path: "/user",
    breadcrumbPath: "/:path",
    breadcrumbParams: "user",
    exact: true,
    sidebar: <span>User Detail</span>,
    main: UserDetail
  }
];

function PrivateRoute({ component: Component, exact, path, auth }) {
  return (
    <Route
      path={path}
      exact={exact}
      render={props =>
        auth.authenticated ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
}

class App extends React.Component {
  render() {
    return (
      <Layout {...this.props}>
        <Breadcrumbs location={this.props.location}></Breadcrumbs>
        <Switch>
          <Route
            path="/"
            exact={true}
            component={Home}
          />
          {routes.map((route, index) => (
            <PrivateRoute
              key={index}
              path={route.path}
              exact={route.exact}
              component={route.main}
              auth={this.props.auth}
            />
          ))}
          <Route path="/login" component={Login} />
        </Switch>
      </Layout>
    )
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth
});

export default withRouter(connect(
  mapStateToProps,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(App));